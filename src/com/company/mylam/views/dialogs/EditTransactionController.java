package com.company.mylam.views.dialogs;

import com.company.mylam.portfolio.Portfolio;
import com.company.mylam.portfolio.fund.Fund;
import com.company.mylam.views.statsviewer.TransactionData;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.joda.time.LocalDate;

/**
 * Created by mannidung on 16/10/14.
 */
public class EditTransactionController {
    @FXML
    private DatePicker transactionDatePicker;

    @FXML
    private ChoiceBox<String> fundChoiceBox;

    @FXML
    private TextField numberOfSharesTextField;

    @FXML
    private TextField valueOfShareTextField;

    @FXML
    private Button cancelButton;

    @FXML
    private Button saveButton;

    @FXML
    private Label messageLabel;

    private Stage dialogStage;
    private boolean okClicked = false;

    private Portfolio portfolio;
    private TransactionData transactionData;

    private boolean editTransaction = true;

    public void setupDialog() {

        // Set message label to be empty
        messageLabel.setText("");

        transactionDatePicker.setValue(jodaTimeDateToJavaDate(transactionData.getDateAsLocalDate()));

        ObservableList fundNamesList = FXCollections.observableArrayList();
        for (Fund f : portfolio) {
            fundNamesList.add(f.getFundName());
        }
        fundChoiceBox.setItems(fundNamesList);
        fundChoiceBox.setValue(transactionData.getFundName());

        if (transactionData.getNumberOfShares().contains("(") || transactionData.getNumberOfShares().contains(")")) {
            numberOfSharesTextField.setPromptText(transactionData.getNumberOfShares());
        } else {
            numberOfSharesTextField.setText(transactionData.getNumberOfShares());
        }

        if (transactionData.getValueOfShare().contains("(") || transactionData.getValueOfShare().contains(")")) {
            valueOfShareTextField.setPromptText(transactionData.getValueOfShare());
        } else {
            valueOfShareTextField.setText(transactionData.getValueOfShare());
        }
    }

    public void handleCancel() {
        dialogStage.close();
    }

    public void handleOk() {
        okClicked = true;

        LocalDate newTransactionDate;
        String newFundName;
        double newNumberOfShares = 0;
        boolean newNumberOfSharesSet = false;
        double newValueOfShare = 0;
        boolean newValueOfShareSet = false;

        newTransactionDate = javaDateToJodaTime(transactionDatePicker.getValue());
        newFundName = fundChoiceBox.getValue();
        try {
            newNumberOfShares = Double.parseDouble(numberOfSharesTextField.getText());
            newNumberOfSharesSet = true;
        } catch (NullPointerException e) {
            System.out.println("Number of shares not set...");
        } catch (NumberFormatException e) {
            System.out.println("You did not specify a proper double...");
        }
        try {
            newValueOfShare = Double.parseDouble(valueOfShareTextField.getText());
            newValueOfShareSet = true;
        } catch (NullPointerException e) {
            System.out.println("Value of share not set...");
        } catch (NumberFormatException e) {
            System.out.println("You did not specify a proper double...");
        }

        // Test the different cases
        if (!newFundName.equals(transactionData.getFundName())) {
            // Only remove if we are editing the transaction, not if we are adding a transaction
            if (editTransaction) {
                portfolio.findFundByName(transactionData.getFundName()).removeNumberOfShares(transactionData.getDateAsLocalDate());
                portfolio.findFundByName(transactionData.getFundName()).removeValueOfShare(transactionData.getDateAsLocalDate());
            }
            if (newNumberOfSharesSet || newValueOfShareSet) {
                if (newNumberOfSharesSet) {
                    portfolio.findFundByName(newFundName).setNumberOfShares(
                            newTransactionDate,
                            newNumberOfShares
                    );
                }
                if (newValueOfShareSet) {
                    portfolio.findFundByName(newFundName).setValueOfShare(
                            newTransactionDate,
                            newValueOfShare
                    );
                }
            } else {
                messageLabel.setText("You need to set at least one value...");
                return;
            }
        } else if (!newTransactionDate.equals(transactionData.getDateAsLocalDate())) {
            // Only remove if we are editing the transaction, not if we are adding a transaction
            if (editTransaction) {
                portfolio.findFundByName(newFundName).removeNumberOfShares(transactionData.getDateAsLocalDate());
                portfolio.findFundByName(newFundName).removeValueOfShare(transactionData.getDateAsLocalDate());
            }
            if (newNumberOfSharesSet || newValueOfShareSet) {
                if (newNumberOfSharesSet) {
                    portfolio.findFundByName(newFundName).setNumberOfShares(
                            newTransactionDate,
                            newNumberOfShares
                    );
                }
                if (newValueOfShareSet) {
                    portfolio.findFundByName(newFundName).setValueOfShare(
                            newTransactionDate,
                            newValueOfShare
                    );
                } else {
                    messageLabel.setText("You need to set at least one value...");
                    return;
                }
            }
        } else {
            if (newNumberOfSharesSet || newValueOfShareSet) {
                if (newNumberOfSharesSet) {
                    portfolio.findFundByName(newFundName).setNumberOfShares(
                            newTransactionDate,
                            newNumberOfShares
                    );
                }
                if (newValueOfShareSet) {
                    portfolio.findFundByName(newFundName).setValueOfShare(
                            newTransactionDate,
                            newValueOfShare
                    );
                }
            } else {
                messageLabel.setText("You need to set at least one value...");
                return;
            }
        }


        dialogStage.close();
    }

    public Stage getDialogStage() {
        return dialogStage;
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    public Portfolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public TransactionData getTransactionData() {
        return transactionData;
    }

    public void setTransactionData(TransactionData transactionData) {
        this.transactionData = transactionData;
    }

    // TODO Move to util class
    private LocalDate javaDateToJodaTime(java.time.LocalDate fromDate) {
        return new LocalDate(fromDate.getYear(), fromDate.getMonthValue(), fromDate.getDayOfMonth());
    }

    // TODO Move to util class
    private java.time.LocalDate jodaTimeDateToJavaDate(LocalDate fromDate) {
        return java.time.LocalDate.of(fromDate.getYear(), fromDate.getMonthOfYear(), fromDate.getDayOfMonth());
    }

    public void setEditTransaction(boolean editTransaction) {
        this.editTransaction = editTransaction;
    }
}
