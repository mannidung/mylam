package com.company.mylam.views.dialogs;

import com.company.mylam.portfolio.Portfolio;
import com.company.mylam.views.portfoliotree.PortfolioTreeController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.time.LocalDate;

/**
 * Created by mannidung on 19/10/14.
 */
public class EditPortfolioController {
    @FXML
    private TextField nameTextField;

    @FXML
    private DatePicker datePicker;

    @FXML
    private Button cancelButton;

    @FXML
    private Button okButton;

    private Stage dialogStage;
    private Portfolio portfolio;
    private PortfolioTreeController portfolioTreeController;
    private boolean okClicked = false;

    public void setupDialog() {
        nameTextField.setText(portfolio.getName());
        datePicker.setValue(jodaTimeDateToJavaDate(portfolio.getEarliestDate()));
    }

    public void handleCancel() {
        dialogStage.close();
    }

    public void handleOk() {
        okClicked = true;

        String newName = nameTextField.getText();
        if (newName.isEmpty()) {
            nameTextField.setPromptText("A name is needed...");
            return;
        } else {
            portfolio.setName(nameTextField.getText());
        }

        // Update portfolio tree
        portfolioTreeController.setPortfolio(portfolio);

        dialogStage.close();
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public Portfolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    public void setPortfolioTreeController(PortfolioTreeController portfolioTreeController) {
        this.portfolioTreeController = portfolioTreeController;
    }

    // TODO Move to util class
    private org.joda.time.LocalDate javaDateToJodaTime(java.time.LocalDate fromDate) {
        return new org.joda.time.LocalDate(fromDate.getYear(), fromDate.getMonthValue(), fromDate.getDayOfMonth());
    }

    // TODO Move to util class
    private java.time.LocalDate jodaTimeDateToJavaDate(org.joda.time.LocalDate fromDate) {
        return java.time.LocalDate.of(fromDate.getYear(), fromDate.getMonthOfYear(), fromDate.getDayOfMonth());
    }
}
