package com.company.mylam.views.dialogs;

import com.company.mylam.portfolio.Portfolio;
import com.company.mylam.portfolio.fund.Fund;
import com.company.mylam.views.portfoliotree.PortfolioTreeController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.joda.time.LocalDate;


/**
 * Created by mannidung on 19/10/14.
 */
public class EditFundController {
    @FXML
    private TextField nameTextField;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private Label messageLabel;

    @FXML
    private Button cancelButton;

    @FXML
    private Button okButton;

    private Stage dialogStage;
    private Portfolio portfolio;
    private Fund fund;
    private PortfolioTreeController portfolioTreeController;
    private boolean okClicked;

    public void setupDialog(Fund fund) {
        this.fund = fund;
        nameTextField.setText(this.fund.getFundName());
        startDatePicker.setValue(jodaTimeDateToJavaDate(this.fund.getStartDate()));
    }

    public void handleCancel() {
        dialogStage.close();
    }

    public void handleOk() {
        okClicked = true;

        String newName = nameTextField.getText();
        if (newName.isEmpty()) {
            nameTextField.setPromptText("A name is needed...");
            return;
        } else {
            fund.setFundName(nameTextField.getText());
        }

        LocalDate newDate = javaDateToJodaTime(startDatePicker.getValue());

        if (newDate.isAfter(fund.getStartDate())) {
            messageLabel.setText("Cannot set new start date after old start date...");
            return;
        } else {
            fund.setStartDate(newDate);
            portfolio.updateEarlieastDate();
        }

        // Update portfolio tree
        portfolioTreeController.setPortfolio(portfolio);

        dialogStage.close();
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public void setPortfolioTreeController(PortfolioTreeController portfolioTreeController) {
        this.portfolioTreeController = portfolioTreeController;
    }

    // TODO Move to util class
    private org.joda.time.LocalDate javaDateToJodaTime(java.time.LocalDate fromDate) {
        return new org.joda.time.LocalDate(fromDate.getYear(), fromDate.getMonthValue(), fromDate.getDayOfMonth());
    }

    // TODO Move to util class
    private java.time.LocalDate jodaTimeDateToJavaDate(org.joda.time.LocalDate fromDate) {
        return java.time.LocalDate.of(fromDate.getYear(), fromDate.getMonthOfYear(), fromDate.getDayOfMonth());
    }
}
