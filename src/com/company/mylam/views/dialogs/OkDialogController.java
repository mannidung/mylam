package com.company.mylam.views.dialogs;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * Created by mannidung on 19/10/14.
 */
public class OkDialogController {
    @FXML
    private Label messageLabel;

    @FXML
    private Label detailsLabel;

    @FXML
    private Button okButton;

    private Stage dialogStage;

    public void setupDialog(String message, String details) {
        messageLabel.setText(message);
        detailsLabel.setText(details);
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    @FXML
    private void handleOK() {
        dialogStage.close();
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
}
