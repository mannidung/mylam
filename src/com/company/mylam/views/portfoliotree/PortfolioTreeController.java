package com.company.mylam.views.portfoliotree;

import com.company.mylam.MyLAM;
import com.company.mylam.portfolio.Portfolio;
import com.company.mylam.portfolio.fund.Fund;
import com.company.mylam.views.statsviewer.StatsViewerController;
import com.company.mylam.views.statsviewer.TransactionData;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import org.joda.time.LocalDate;

import java.util.Random;

/**
 * Created by mannidung on 12/09/14.
 */
public class PortfolioTreeController {
    @FXML
    private TreeView<String> portfolioTreeView;

    private StatsViewerController statsViewerController;

    Portfolio portfolio;
    private MyLAM mylam;

    ContextMenu cm;


    /**
     * Initialize the portfolio tree controller
     */
    public void initialize() {
        //generateTestPortfolio();

        // Set tree view to checkboxtree
        portfolioTreeView.setCellFactory(CheckBoxTreeCell.<String>forTreeView());

        // Set a listener to the selection
        //portfolioTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> selectionListener(oldValue, newValue));

    }

    /**
     * Fills the tree with the strings specified in loadTreeItems
     */
    public void loadPortfolio() {

        CheckBoxTreeItem<String> root = new CheckBoxTreeItem<String>(portfolio.getName());

        // Add a listener to the checkBoxTreeItem
        root.selectedProperty().addListener((observable, oldValue, newValue) -> checkBoxTreeItemListener(root, observable, oldValue, newValue));
        // Expand the tree and set the selection to true
        root.setExpanded(true);
        root.setSelected(true);
        for (Fund fund: portfolio) {
            CheckBoxTreeItem<String> cbi = new CheckBoxTreeItem<String>(fund.getFundName());

            // Set the checkbox marked as true
            cbi.setSelected(true);

            // Add to the tree
            root.getChildren().add(cbi);

            // Add a listener to the checkBoxTreeItem
            cbi.selectedProperty().addListener((observable, oldValue, newValue) -> checkBoxTreeItemListener(cbi, observable, oldValue, newValue));
        }
        portfolioTreeView.setRoot(root);

        portfolioTreeView.setOnMouseClicked((event) -> handleFundClicked(event));
    }

    /**
     * Prints out the information returned from the listener
     * @param cbi The CheckBoxTreeItem
     * @param o The observableValue
     * @param oldValue The old value of the checkbox
     * @param newValue The new value of the checkbox
     */
    private void checkBoxTreeItemListener(CheckBoxTreeItem<String> cbi, ObservableValue o, Boolean oldValue, Boolean newValue) {
        try {
            Fund foundFund = portfolio.findFundByName(cbi.getValue());
            System.out.println("Old value for " + cbi.getValue() + " was: " + oldValue + ", new value is: " + newValue);
            System.out.println("ISIN of the fund is: " + foundFund.getIsin() +
                    " and name is " + foundFund.getFundName());
            foundFund.setShouldPlot(newValue);
            statsViewerController.updateActiveController(portfolio);
            //addRemoveDataFromChart(newValue, foundFund, valuePerFundAreaChart);
            //addRemoveDataFromChart(newValue, foundFund, percentagePerFundStackedAreaChart);
        } catch (NullPointerException e) {
            // TODO well, this catches the exception when started and when no funds are checked
        }
    }

    private void handleFundClicked(MouseEvent event ) {
        //region DEBUG
        System.out.println("Event registered: " + event.getButton().toString());
        //endregion

        try {
            cm.hide();
        } catch (NullPointerException e) {
            // This is just to catch an expected exception
        }

        if (event.getButton() == MouseButton.SECONDARY) {
            String clickedItem = portfolioTreeView.getSelectionModel().getSelectedItem().getValue();
            //region DEBUG
            System.out.println("Selected item has name: " + clickedItem);
            System.out.println("It is a leaf: " + portfolioTreeView.getSelectionModel().getSelectedItem().isLeaf());
            //endregion


            cm = new ContextMenu();
            MenuItem menuItemEdit;
            MenuItem menuItemDelete;
            MenuItem menuItemAdd;

            if (portfolioTreeView.getSelectionModel().getSelectedItem().isLeaf()) {
                menuItemAdd = new MenuItem("Add transaction");
                menuItemEdit = new MenuItem("Edit fund");
                menuItemDelete = new MenuItem("Delete fund");
                menuItemAdd.setOnAction((editFundEvent) -> mylam.showAddTransactionDialog(portfolioTreeView.getSelectionModel().getSelectedItem().getValue()));
                menuItemEdit.setOnAction((editFundEvent) -> mylam.showEditFundDialog(portfolioTreeView.getSelectionModel().getSelectedItem().getValue()));
                menuItemDelete.setOnAction((deleteFundEvent) -> mylam.showDeleteFundDialog(portfolioTreeView.getSelectionModel().getSelectedItem().getValue()));
                cm.getItems().addAll(menuItemAdd, menuItemEdit, menuItemDelete);
            } else {
                menuItemEdit = new MenuItem("Edit portfolio");
                menuItemAdd = new MenuItem("Add fund to portfolio");
                menuItemEdit.setOnAction((editTransactionEvent) -> mylam.showEditPortfolioDialog());
                menuItemAdd.setOnAction((addFundEvent) -> mylam.showAddFundDialog());
                cm.getItems().addAll(menuItemAdd, menuItemEdit);
            }

            cm.show(portfolioTreeView, event.getScreenX(), event.getScreenY());
        }
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;

        // Load in the test items
        loadPortfolio();
    }

    public void setStatsViewerController(StatsViewerController statsViewerController) {
        this.statsViewerController = statsViewerController;
    }

    public MyLAM getMylam() {
        return mylam;
    }

    public void setMylam(MyLAM mylam) {
        this.mylam = mylam;
    }
}
