package com.company.mylam.views;

import com.company.mylam.MyLAM;
import com.company.mylam.portfolio.Portfolio;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;

/**
 * Created by mannidung on 08/10/14.
 */
public class RootLayoutController {

    @FXML
    private MenuItem saveMenuItem;

    @FXML
    private MenuItem saveAsMenuItem;

    private Portfolio portfolio;
    private MyLAM mylam;

    // Wrapper method for mylam.newPortfolio();
    public void newPortfolio(){
        mylam.newPortfolio();
    }

    // Wrapper method for mylam.saveAsPortfolio();
    public void savePortfolio(){
        mylam.savePortfolio();
    }

    // Wrapper method for mylam.saveAsPortfolio();
    public void saveAsPortfolio(){
        mylam.saveAsPortfolio();
    }

    // Wrapper method for mylam.loadPortfolio();
    public void loadPortfolio(){
        mylam.loadPortfolio();
    }

    public void closeMyLAM(){
        //region DEBUG
        System.out.println("closeMyLAM() called");
        //region

        mylam.closeApplication();
    }

    public void setPortfolio(Portfolio portfolio){
        this.portfolio = portfolio;
    }

    public void setMylam(MyLAM mylam){
        this.mylam = mylam;
    }

    public void setSaveMenuItemDisabled(boolean disabled) {
        saveMenuItem.setDisable(disabled);
    }

    public void setSaveAsMenuItemDisabled(boolean disabled) {
        saveAsMenuItem.setDisable(disabled);
    }
}
