package com.company.mylam.views.statsviewer;

import com.company.mylam.portfolio.Portfolio;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by mannidung on 14/09/14.
 */
public class EarningsController extends AbstractBorderPaneController {

    @FXML
    private BarChart<String, Number> earningsChart;

    @FXML
    private CategoryAxis earningsChartXAxis;

    @FXML
    private NumberAxis earningsChartYAxis;

    private Portfolio portfolio;

    public void initialize() {

    }

    private void earningsGraph(LocalDate startDate, LocalDate stopDate, String resolution) {
        // Define a series
        XYChart.Series series = new XYChart.Series();
        series.setName("My Portfolio");

        double value = 0;
        double minValue = 0;
        double maxValue = 0;
        if (resolution.equals("Monthly")) {
            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("y-M");

            // Gets the end of the first month
            LocalDate endOfFirstMonth = startDate.dayOfMonth().withMaximumValue();

            value = portfolio.getChangeBetweenDates(startDate,
                    endOfFirstMonth);

            series.getData().add(new XYChart.Data(startDate.toString(dateFormatter),
                    value));

            minValue = value;
            maxValue = value;

            // Generate data
            int i = 0;
            while (endOfFirstMonth.plusMonths(i).isBefore(stopDate)) {

                value = portfolio.getChangeBetweenDates(endOfFirstMonth.plusMonths(i - 1),
                        endOfFirstMonth.plusMonths(i));

                series.getData().add(new XYChart.Data(endOfFirstMonth.plusMonths(
                        i).toString(dateFormatter), value));

                if (value < minValue) {
                    minValue = value;
                }
                if (value > maxValue) {
                    maxValue = value;
                }

                i++;
            }
        } else if (resolution.equals("Weekly")) {
            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("y-w");

            // Gets the end of the first week
            LocalDate endOfFirstWeek = startDate.dayOfWeek().withMaximumValue();

            value = portfolio.getChangeBetweenDates(startDate,
                    endOfFirstWeek);

            series.getData().add(new XYChart.Data(startDate.toString(dateFormatter),
                    value));

            minValue = value;
            maxValue = value;

            // Generate data
            int i = 0;
            while (endOfFirstWeek.plusWeeks(i).isBefore(stopDate)) {
                value = portfolio.getChangeBetweenDates(endOfFirstWeek.plusWeeks(i - 1),
                        endOfFirstWeek.plusWeeks(i));
                System.out.println(value);

                series.getData().add(new XYChart.Data(endOfFirstWeek.plusWeeks(
                        i).toString(dateFormatter), value));

                if (value < minValue) {
                    minValue = value;
                }
                if (value > maxValue) {
                    maxValue = value;
                }

                i++;
            }
        } else if (resolution.equals("Daily")) {
            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("y-M-d");

            // Generate data
            int i = 1;
            while (startDate.plusDays(i).isBefore(stopDate)) {

                value = portfolio.getChangeBetweenDates(startDate.plusDays(i - 1),
                        startDate.plusDays(i));

                series.getData().add(new XYChart.Data(startDate.plusDays(
                        i).toString(dateFormatter), value));

                if (i == 1) {
                    minValue = value;
                    maxValue = value;
                }

                if (value < minValue) {
                    minValue = value;
                }
                if (value > maxValue) {
                    maxValue = value;
                }

                i++;
            }
        }



        System.out.println("MinValue: " + minValue);
        System.out.println("MaxValue: " + maxValue);

        earningsChartYAxis.setAutoRanging(false);
        earningsChartYAxis.setLowerBound(minValue - 5);
        earningsChartYAxis.setUpperBound(maxValue + 5);
        earningsChartYAxis.setTickUnit((maxValue - minValue)/20);

        // To keep the number of X-label entries to maximum 10
        int length = series.getData().size();
        if (length > 10) {
            String invisible = " ";
            int i = 0;
            for (BarChart.Data data : (ObservableList<BarChart.Data<String, Number>>) series.getData()) {
                if (i%(length/10) != 0) {
                    data.setXValue(invisible);
                    invisible += (char)29;
                }
                i++;
            }
        }

        earningsChart.setTitle("Value change of Portfolio");
        earningsChart.setLegendVisible(false);
        earningsChart.getData().add(series);
        earningsChart.setCategoryGap(0);
    }

    public void setupPane(Portfolio portfolio, LocalDate startDate, LocalDate stopDate, String resolution) {
        this.portfolio = portfolio;
        earningsChart.getData().clear();
        earningsGraph(startDate, stopDate, resolution);
    }
}
