package com.company.mylam.views.statsviewer;

import com.company.mylam.portfolio.fund.Fund;
import javafx.beans.property.SimpleStringProperty;
import org.joda.time.LocalDate;

import java.text.DecimalFormat;

/**
 * Created by mannidung on 23/09/14.
 */
public class TransactionData {
    private final SimpleStringProperty date;
    private final SimpleStringProperty fundName;
    private final SimpleStringProperty numberOfSharesBought;
    private double numberOfSharesBoughtDouble;
    private final SimpleStringProperty numberOfShares;
    private double numberOfSharesDouble;
    private final SimpleStringProperty valueOfShare;
    private double valueOfShareDouble;
    private final SimpleStringProperty generatedProfit;
    private double generatedProfitDouble;
    private final SimpleStringProperty valueOfFund;
    private double valueOfFundDouble;

    private LocalDate periodEnd;
    private DecimalFormat df = new DecimalFormat("#.00");

    public TransactionData(LocalDate date, Fund f, LocalDate periodEnd) {
        this.date = new SimpleStringProperty(date.toString());
        this.fundName = new SimpleStringProperty(f.getFundName());
        this.periodEnd = periodEnd;

        numberOfSharesDouble = f.getNumberOfShares(date);
        valueOfShareDouble = f.getValueOfShare(date);
        valueOfFundDouble = f.getTotalValueOfAsset(date);



        // Compute the number of shares bought
            if (date.equals(f.getStartDate())) {
                numberOfSharesBoughtDouble = f.getNumberOfShares(date);
            } else {
                numberOfSharesBoughtDouble = f.getNumberOfShares(date)
                        - f.getNumberOfShares(date.minusDays(1));
            }

        // Compute the generated profit of each investment
        if(f.numberOfSharesSetAt(date)) {
            generatedProfitDouble = numberOfSharesBoughtDouble*f.getValueOfShare(periodEnd)
                        - numberOfSharesBoughtDouble*f.getValueOfShare(date);
        } else {
        }


        if (f.numberOfSharesSetAt(date)) {
            this.numberOfSharesBought = new SimpleStringProperty(df.format(numberOfSharesBoughtDouble));
        } else {
            this.numberOfSharesBought = new SimpleStringProperty("---");
        }

        if(f.numberOfSharesSetAt(date) && f.valueOfShareSetAt(date)) {
            this.numberOfShares = new SimpleStringProperty(df.format(numberOfSharesDouble));
            this.valueOfShare = new SimpleStringProperty(df.format(valueOfShareDouble));
        } else if(f.numberOfSharesSetAt(date)) {
            this.numberOfShares = new SimpleStringProperty(df.format(numberOfSharesDouble));
            this.valueOfShare = new SimpleStringProperty("(" + df.format(valueOfShareDouble) + ")");
        } else if (f.valueOfShareSetAt(date)){
            this.numberOfShares = new SimpleStringProperty("(" + df.format(numberOfSharesDouble) + ")");
            this.valueOfShare = new SimpleStringProperty(df.format(valueOfShareDouble));
        } else {
            this.numberOfShares = new SimpleStringProperty("(" + df.format(0) + ")");
            this.valueOfShare = new SimpleStringProperty("(" + df.format(0) + ")");
        }
        this.valueOfFund = new SimpleStringProperty(df.format(valueOfFundDouble));
        if (f.numberOfSharesSetAt(date)) {
            if (numberOfSharesBoughtDouble <= 0) {
                this.generatedProfit = new SimpleStringProperty("---");
            } else {
                this.generatedProfit = new SimpleStringProperty(df.format(generatedProfitDouble));
            }
        } else {
            this.generatedProfit = new SimpleStringProperty("---");
        }
    }

    public String getDate() {
        return date.get();
    }

    public LocalDate getDateAsLocalDate() {
        return new LocalDate(date.getValue());
    }

    public void setDate(LocalDate date) {
        this.date.set(date.toString());
    }

    public String getFundName() {
        return fundName.get();
    }

    public void setFundName(String fundName) {
        this.fundName.set(fundName);
    }

    public String getNumberOfShares() {
        return numberOfShares.get();
    }

    public void setNumberOfShares(int numberOfShares) {
        if (numberOfShares == 0) {
            this.numberOfShares.set("");
        } else {
            this.numberOfShares.set(Integer.toString(numberOfShares));
        }

    }

    public String getValueOfShare() {
        return valueOfShare.get();
    }

    public void setValueOfShare(double valueOfShare) {
        if (valueOfShare == 0) {
            this.valueOfShare.set("");
        } else {
            this.valueOfShare.set(df.format(valueOfShare));
        }

    }

    public String getValueOfFund() {
        return valueOfFund.get();
    }

    public void setValueOfFund(Double valueOfFund) {
        this.valueOfFund.set(df.format(valueOfFund));
    }

    public String getGeneratedProfit() {
        return generatedProfit.get();
    }


    public void setGeneratedProfit(Double generatedProfit) {
        this.generatedProfit.set(df.format(generatedProfit));
    }

    public void print() {
        System.out.println("Date: " + date.getValue());
        System.out.println("Fund name: " +fundName.getValue());
        System.out.println("Number of shares: " + numberOfShares.getValue());
        System.out.println("Value of shares: " + valueOfShare.getValue());
        System.out.println("Generated profit: " + generatedProfit.getValue());
    }

    public String getNumberOfSharesBought() {
        return numberOfSharesBought.get();
    }
}
