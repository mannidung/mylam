package com.company.mylam.views.statsviewer;

import com.company.mylam.portfolio.Portfolio;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by mannidung on 14/09/14.
 */
public class ValueChangeController extends AbstractBorderPaneController {

    @FXML
    private LineChart<String, Number> valueChangeChart;

    @FXML
    private CategoryAxis valueChangeChartXAxis;

    @FXML
    private NumberAxis valueChangeChartYAxis;


    private Portfolio portfolio;

    public void initialize() {

    }

    private void valueChangeLineGraph(LocalDate startDate, LocalDate stopDate, String resolution) {

        // Define a series
        XYChart.Series series = new XYChart.Series();
        series.setName("My Portfolio");
        valueChangeChart.setTitle("Value change of Portfolio");
        valueChangeChart.setLegendVisible(false);

        // For the chart
        double value = 0;
        double minValue = 0;
        double maxValue = 0;

        if (resolution.equals("Yearly")) {
            //TODO Still to be implemented
        } else if (resolution.equals("Monthly")) {
            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("y-M");

            // Gets the end of the first month
            LocalDate endOfFirstMonth = startDate.dayOfMonth().withMaximumValue();

            // Generate data
            int i = 0;
            while (startDate.plusMonths(i).isBefore(stopDate)) {

                value = portfolio.getChangeBetweenDates(endOfFirstMonth.plusMonths(i-1), endOfFirstMonth.plusMonths(i));

                series.getData().add(new XYChart.Data(endOfFirstMonth.plusMonths(
                        i).toString(dateFormatter), value));

                if (value < minValue) {
                    minValue = value;
                }
                if (value > maxValue) {
                    maxValue = value;
                }

                i++;
            }
        } else if (resolution.equals("Weekly")) {
            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("y-w");

            // Get the end of the first week
            LocalDate endOfFirstWeek = startDate.dayOfWeek().withMaximumValue();

            // Generate data
            int i = 0;
            while (startDate.plusWeeks(i).isBefore(stopDate)) {

                value = portfolio.getChangeBetweenDates(endOfFirstWeek.plusWeeks(i-1), endOfFirstWeek.plusWeeks(i));

                series.getData().add(new XYChart.Data(endOfFirstWeek.plusWeeks(i).toString(dateFormatter),
                        value));

                if (value < minValue) {
                    minValue = value;
                }
                if (value > maxValue) {
                    maxValue = value;
                }

                i++;
            }
        } else if (resolution.equals("Daily")) {
            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("y-M-d");

            // Generate data
            int i = 1;
            while (startDate.plusDays(i).isBefore(stopDate)) {

                value = portfolio.getChangeBetweenDates(startDate.plusDays(i-1), startDate.plusDays(i));

                series.getData().add(new XYChart.Data(startDate.plusDays(i).toString(dateFormatter),
                         value));

                if (value < minValue) {
                    minValue = value;
                }
                if (value > maxValue) {
                    maxValue = value;
                }

                i++;
            }

            value = portfolio.getChangeBetweenDates(startDate.plusDays(i), stopDate);

            if (value < minValue) {
                minValue = value;
            }
            if (value > maxValue) {
                maxValue = value;
            }

            series.getData().add(new XYChart.Data(stopDate.toString(dateFormatter), value));
        }

        valueChangeChartYAxis.setAutoRanging(false);
        valueChangeChartYAxis.setLowerBound(minValue);
        valueChangeChartYAxis.setUpperBound(maxValue);
        valueChangeChartYAxis.setTickUnit((maxValue - minValue)/20);
        valueChangeChartXAxis.setTickMarkVisible(false);

        // To keep the number of X-label entries to maximum 10
        int length = series.getData().size();
        if (length > 10) {
            String invisible = " ";
            int i = 0;
            for (XYChart.Data data : (ObservableList<XYChart.Data<String, Number>>) series.getData()) {
                if (i%(length/10) != 0) {
                    data.setXValue(invisible);
                    invisible += (char)29;
                }
                i++;
            }
        }

        valueChangeChart.getData().add(series);
    }

    public void setupPane(Portfolio portfolio, LocalDate startDate, LocalDate stopDate, String resolution) {
        this.portfolio = portfolio;
        valueChangeChart.getData().clear();
        valueChangeLineGraph(startDate, stopDate, resolution);
    }
}
