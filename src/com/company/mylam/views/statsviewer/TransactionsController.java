package com.company.mylam.views.statsviewer;

import com.company.mylam.MyLAM;
import com.company.mylam.portfolio.Portfolio;
import com.company.mylam.portfolio.fund.Fund;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.*;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import org.joda.time.LocalDate;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created by mannidung on 14/09/14.
 */
public class TransactionsController extends AbstractBorderPaneController{
    @FXML
    private TableView transactionTable;

    @FXML
    private TableColumn transactionDateColumn;

    @FXML
    private TableColumn fundNameColumn;

    @FXML
    private TableColumn numberOfSharesBoughtColumn;

    @FXML
    private TableColumn numberOfSharesColumn;

    @FXML
    private TableColumn shareValueColumn;

    @FXML
    private TableColumn fundValueColumn;

    @FXML
    private TableColumn generatedProfitColumn;

    // For adding transaction
    @FXML
    private DatePicker newTransactionDatePicker;

    @FXML
    private ChoiceBox<String> newTransactionChoiceBox;

    @FXML
    private TextField newTransactionNumberOfShares;

    @FXML
    private TextField newTransactionValueOfShare;

    @FXML
    private Button newTransactionAdd;

    // Variables for updating the tables
    private LocalDate currentStartDate;
    private LocalDate currentStopDate;
    private String currentResolution;

    // Variable to keep track of the choice box
    private int choiceBoxIndex = 0;

    final ObservableList<TransactionData> tableData = FXCollections.observableArrayList();

    // Local Portfolio variable
    private Portfolio portfolio;

    // Variable for the transaction table context menu
    ContextMenu cm;

    public void initialize() {

    }

    private void populateTable(LocalDate startDate, LocalDate stopDate) {
        for (Fund f : portfolio) {

            LocalDate thisDate = startDate;
            while (thisDate.isBefore(stopDate) ||
                    thisDate.isEqual(stopDate)) {
                // If at least one of either the shares or the value of the share is set, create table data entry
                if(f.numberOfSharesSetAt(thisDate) || f.valueOfShareSetAt(thisDate)) {
                    tableData.add(
                            new TransactionData(thisDate, f, stopDate)
                    );
                }
                thisDate = thisDate.plusDays(1);
            }
        }

    }

    private void setupTransactionsTable(LocalDate startDate, LocalDate stopDate) {
        transactionDateColumn.setCellValueFactory(new PropertyValueFactory<TransactionData, String>("date"));
        fundNameColumn.setCellValueFactory(new PropertyValueFactory<TransactionData, String>("fundName"));
        numberOfSharesBoughtColumn.setCellValueFactory(new PropertyValueFactory<TransactionData, String>("numberOfSharesBought"));
        numberOfSharesColumn.setCellValueFactory(new PropertyValueFactory<TransactionData, String>("numberOfShares"));
        shareValueColumn.setCellValueFactory(new PropertyValueFactory<TransactionData, String>("valueOfShare"));
        fundValueColumn.setCellValueFactory(new PropertyValueFactory<TransactionData, String>("valueOfFund"));
        generatedProfitColumn.setCellValueFactory(new PropertyValueFactory<TransactionData, String>("generatedProfit"));

        populateTable(startDate, stopDate);

        fundValueColumn.setEditable(false);

        transactionTable.setItems(tableData);

        // Set sorting to transaction date
        transactionTable.getSortOrder().clear();
        transactionTable.getSortOrder().add(transactionDateColumn);

    }

    private void setupPane() {
        setupPane(this.portfolio, currentStartDate, currentStopDate, currentResolution);
    }

    public void setupPane(Portfolio portfolio, LocalDate startDate, LocalDate stopDate, String resolution) {
        this.currentStartDate = startDate;
        this.currentStopDate = stopDate;
        this.portfolio = portfolio;
        tableData.clear();
        setupTransactionsTable(startDate, stopDate);
        ObservableList fundNamesList = FXCollections.observableArrayList();
        for (Fund f : portfolio) {
            fundNamesList.add(f.getFundName());
        }

        newTransactionChoiceBox.setItems(fundNamesList);
        newTransactionChoiceBox.setValue(fundNamesList.get(choiceBoxIndex).toString());

        // Set default value of date picker to today
        newTransactionDatePicker.setValue(java.time.LocalDate.now());

        // Set listener for the table in order to handle edit of transactions
        //transactionTable.getSelectionModel().selectedItemProperty().addListener(
        //        (observable, oldValue, newValue) -> handleEditTransaction());

        transactionTable.setOnMouseClicked((event) -> handleTransactionTableClicked(event));

        //transactionDateColumn.addEventHandler(
        //        javafx.scene.input.MouseEvent.MOUSE_CLICKED, (event) -> handleEditTransaction(event));

        handleTransactionChoiceBoxItemChanged(fundNamesList.get(choiceBoxIndex).toString());

        newTransactionChoiceBox.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> handleTransactionChoiceBoxItemChanged(newValue)
        );
        newTransactionValueOfShare.focusedProperty().addListener(
                (observable, oldValue, newValue) -> convertPromptTextToText(newTransactionValueOfShare, newValue)
        );
        newTransactionNumberOfShares.focusedProperty().addListener(
                (observable, oldvalue, newValue) -> convertPromptTextToText(newTransactionNumberOfShares, newValue)
        );
    }

    private void handleTransactionChoiceBoxItemChanged(String fundName) {
        //region DEBUG
        System.out.println("handleTransactionChoiceBoxItemChanged called with fund name " + fundName);
        //endregion

        double numberOfShares = portfolio.findFundByName(fundName).getNumberOfShares(
                javaDateToJodaTime(newTransactionDatePicker.getValue())
        );

        double shareValue = portfolio.findFundByName(fundName).getValueOfShare(
                javaDateToJodaTime(newTransactionDatePicker.getValue())
        );

        newTransactionValueOfShare.clear();
        newTransactionNumberOfShares.clear();
        
        newTransactionValueOfShare.setPromptText(Double.toString(shareValue));
        newTransactionNumberOfShares.setPromptText(Double.toString(numberOfShares));
    }

    private void convertPromptTextToText(TextField textField, boolean isFocused) {
        if (isFocused) {
            if (textField.getText().equals("")) {
                textField.setText(textField.getPromptText());
            }
        } else {
            if (textField.getPromptText().equals(textField.getText())) {
                textField.setPromptText(textField.getText());
                textField.clear();
            }

        }
    }

    private void handleTransactionTableClicked(javafx.scene.input.MouseEvent event ) {
        //region DEBUG
        System.out.println("Event registered: " + event.getButton().toString());
        //endregion

        try {
            cm.hide();
        } catch (NullPointerException e) {
            // This is just to catch an expected exception
        }

        if (event.getButton() == MouseButton.SECONDARY) {
            TransactionData transactionData = (TransactionData) transactionTable.getSelectionModel().getSelectedItem();
            //region DEBUG
            System.out.println("Selected item has the following properties:");
            transactionData.print();
            //endregion


            cm = new ContextMenu();
            MenuItem menuItemEdit = new MenuItem("Edit transaction");
            MenuItem menuItemDelete = new MenuItem("Delete transaction");

            menuItemEdit.setOnAction((editTransactionEvent) -> handleEditTransaction(transactionData));
            menuItemDelete.setOnAction((editTransactionEvent) -> handleDeleteTransaction(transactionData));
            cm.getItems().addAll(menuItemEdit, menuItemDelete);

            cm.show(transactionTable, event.getScreenX(), event.getScreenY());

        }
    }

    private void handleDeleteTransaction(TransactionData transactionData) {
        //region DEBUG
        System.out.println("handleDeleteTransaction() called...");
        //endregion

        //region DEBUG
        System.out.println("Date of transactionData: " + transactionData.getDateAsLocalDate().toString());
        //endregion

        LocalDate dateOfTransaction = transactionData.getDateAsLocalDate();
        this.portfolio.findFundByName(transactionData.getFundName()).removeNumberOfShares(dateOfTransaction);
        this.portfolio.findFundByName(transactionData.getFundName()).removeValueOfShare(dateOfTransaction);

        mylam.setPortfolioChanged(true);

        // Update transaction table
        setupPane();
    }

    private void handleEditTransaction(TransactionData transactionData) {
        boolean editSuccessful = false;

        //region DEBUG
        System.out.println("handleEditTransaction() called...");
        //endregion

        //region DEBUG
        System.out.println("Date of transactionData: " + transactionData.getDateAsLocalDate().toString());
        //endregion

        editSuccessful = getMylam().showEditTransactionDialog(transactionData);
        if (editSuccessful) {
            setupPane();
        }

        //region DEBUG
        System.out.println("editSuccessful: " + editSuccessful);
        //endregion
    }

    public void addTransaction() {
        //region DEBUG
        System.out.println("addTransaction() called");
        //endregion

        LocalDate transactionDate = null;
        Fund transactionFund = null;
        double transactionNumberOfShares = 0;
        double transactionValueOfShares = 0;
        boolean transactionNumberOfSharesSet = false;
        boolean transactionValueOfSharesSet = false;

        try {
            transactionDate = javaDateToJodaTime(newTransactionDatePicker.getValue());
        } catch (NullPointerException e) {
            System.out.println("Date not set...");
            return;
        }
        try {
            transactionFund = portfolio.findFundByName(newTransactionChoiceBox.getValue());
        } catch (NullPointerException e) {
            System.out.println("Fund is not set...");
            return;
        }
        try {
            transactionNumberOfShares = Double.parseDouble(newTransactionNumberOfShares.getText());
            transactionNumberOfSharesSet = true;
        } catch (NullPointerException e) {
            System.out.println("Number of shares not set...");
        } catch (NumberFormatException e) {
            System.out.println("You did not specify a proper double...");
        }
        try {
            transactionValueOfShares = Double.parseDouble(newTransactionValueOfShare.getText());
            transactionValueOfSharesSet = true;
        } catch (NullPointerException e) {
            System.out.println("Value of share not set...");
        } catch (NumberFormatException e) {
            System.out.println("You did not specify a proper double...");
        }
        if (transactionNumberOfSharesSet || transactionValueOfSharesSet) {
            if (transactionNumberOfSharesSet) {
                System.out.println("Number of shares set to " + transactionNumberOfShares
                        + " at date " + transactionDate
                        + " for fund " + transactionFund.getFundName());
                transactionFund.setNumberOfShares(transactionDate, transactionNumberOfShares);
            }
            if (transactionValueOfSharesSet) {
                System.out.println("Value of share set to " + transactionValueOfShares
                        + " at date " + transactionDate
                        + " for fund " + transactionFund.getFundName());
                transactionFund.setValueOfShare(transactionDate, transactionValueOfShares);
            }
        } else {
            System.out.println("At least one of the number of shares or value of share needs to be set...");
            return;
        }
        mylam.setPortfolioChanged(true);
        setChoiceBoxIndex();
        setupPane();
        newTransactionDatePicker.requestFocus();
    }

    public void setChoiceBoxIndex() {
        choiceBoxIndex = newTransactionChoiceBox.getItems().indexOf(newTransactionChoiceBox.getValue());
    }

    @FXML
    private void onEnter() {
        //region DEBUG
        System.out.println("onEnter() called");
        //endregion
        addTransaction();
    }

    // TODO Move to util class
    private LocalDate javaDateToJodaTime(java.time.LocalDate fromDate) {
        return new LocalDate(fromDate.getYear(), fromDate.getMonthValue(), fromDate.getDayOfMonth());
    }

    // TODO Move to util class
    private java.time.LocalDate jodaTimeDateToJavaDate(LocalDate fromDate) {
        return java.time.LocalDate.of(fromDate.getYear(), fromDate.getMonthOfYear(), fromDate.getDayOfMonth());
    }
}
