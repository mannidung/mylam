package com.company.mylam.views.statsviewer;

import com.company.mylam.MyLAM;
import com.company.mylam.portfolio.Portfolio;
import org.joda.time.LocalDate;

/**
 * Created by mannidung on 17/09/14.
 */
abstract public class AbstractBorderPaneController {

    protected MyLAM mylam;

    abstract public void setupPane(Portfolio portfolio, LocalDate startDate, LocalDate stopDate, String resolution);

    public MyLAM getMylam() {
        return mylam;
    }

    public void setMylam(MyLAM mylam) {
        this.mylam = mylam;
    }
}
