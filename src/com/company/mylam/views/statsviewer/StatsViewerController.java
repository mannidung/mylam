package com.company.mylam.views.statsviewer;

import com.company.mylam.MyLAM;
import com.company.mylam.portfolio.Portfolio;
import com.company.mylam.portfolio.saveclasses.SaveUtils;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import org.joda.time.LocalDate;

import java.io.File;
import java.io.IOException;

/**
 * Created by mannidung on 13/09/14.
 */
public class StatsViewerController {

    // The border pane of the statsviewer
    @FXML
    private BorderPane thisBorderPane;

    @FXML
    private Button overviewButton;

    @FXML
    private Button transactionsButton;

    @FXML
    private Button valueChangeButton;

    @FXML
    private Button compositionButton;

    @FXML
    private Button earningsButton;

    @FXML
    private Button statsButton;

    @FXML
    protected DatePicker periodStartDatePicker;

    @FXML
    protected DatePicker periodStopDatePicker;

    @FXML
    protected ChoiceBox<String> resolutionChoiceBox;

    // The border pane to be place in the center of the
    private BorderPane loadedBorderPane;

    // The current active controller
    private AbstractBorderPaneController activeController;


    // Controllers
    CompositionController compositionController;
    EarningsController earningsController;
    OverviewController overviewController;
    StatisticsController statisticsController;
    TransactionsController transactionsController;
    ValueChangeController valueChangeController;

    Portfolio portfolio;

    // Referene to main class
    private MyLAM mylam;

    /**
     * Initialize the portfolio tree controller
     */
    public void initialize() {
        resolutionChoiceBox.setItems(FXCollections.observableArrayList(
                "Monthly",
                "Weekly",
                "Daily"));
        resolutionChoiceBox.setValue("Monthly");
    }

    private void initializeCenterBorderPane() {
        switchCenterBorderPaneToOverview();
    }

    @FXML
    private void switchCenterBorderPaneToOverview() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/statsviewer/Overview.fxml"));
            loadedBorderPane = (BorderPane) loader.load();

            thisBorderPane.setCenter(loadedBorderPane);

            overviewController = loader.getController();
            overviewController.setupPane(portfolio,
                    javaDateToJodaTime(periodStartDatePicker.getValue()),
                    javaDateToJodaTime(periodStopDatePicker.getValue()),
                    resolutionChoiceBox.getValue());
            activeController = overviewController;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void switchCenterBorderPaneToTransactions() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/statsviewer/Transactions.fxml"));
            loadedBorderPane = (BorderPane) loader.load();

            thisBorderPane.setCenter(loadedBorderPane);

            transactionsController = loader.getController();
            transactionsController.setupPane(portfolio,
                    javaDateToJodaTime(periodStartDatePicker.getValue()),
                    javaDateToJodaTime(periodStopDatePicker.getValue()),
                    resolutionChoiceBox.getValue());

            // Set the reference to the main class in the controller
            transactionsController.setMylam(mylam);

            // Update the active controller
            activeController = transactionsController;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void switchCenterBorderPaneToValueChange() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/statsviewer/ValueChange.fxml"));
            loadedBorderPane = (BorderPane) loader.load();

            thisBorderPane.setCenter(loadedBorderPane);

            valueChangeController = loader.getController();
            valueChangeController.setupPane(portfolio,
                    javaDateToJodaTime(periodStartDatePicker.getValue()),
                    javaDateToJodaTime(periodStopDatePicker.getValue()),
                    resolutionChoiceBox.getValue());
            activeController = valueChangeController;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void switchCenterBorderPaneToComposition() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/statsviewer/Composition.fxml"));
            loadedBorderPane = (BorderPane) loader.load();

            thisBorderPane.setCenter(loadedBorderPane);

            compositionController = loader.getController();
            compositionController.setupPane(portfolio,
                    javaDateToJodaTime(periodStartDatePicker.getValue()),
                    javaDateToJodaTime(periodStopDatePicker.getValue()),
                    resolutionChoiceBox.getValue());
            activeController = compositionController;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void switchCenterBorderPaneToEarnings() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/statsviewer/Earnings.fxml"));
            loadedBorderPane = (BorderPane) loader.load();

            thisBorderPane.setCenter(loadedBorderPane);

            earningsController = loader.getController();
            earningsController.setupPane(portfolio,
                    javaDateToJodaTime(periodStartDatePicker.getValue()),
                    javaDateToJodaTime(periodStopDatePicker.getValue()),
                    resolutionChoiceBox.getValue());
            activeController = earningsController;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void switchCenterBorderPaneToStatistics() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/statsviewer/Statistics.fxml"));
            loadedBorderPane = (BorderPane) loader.load();

            thisBorderPane.setCenter(loadedBorderPane);

            statisticsController = loader.getController();
            statisticsController.setupPane(portfolio,
                    javaDateToJodaTime(periodStartDatePicker.getValue()),
                    javaDateToJodaTime(periodStopDatePicker.getValue()),
                    resolutionChoiceBox.getValue());
            activeController = statisticsController;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateActiveController(Portfolio portfolio) {
        updateActiveController(portfolio,
                javaDateToJodaTime(periodStartDatePicker.getValue()),
                javaDateToJodaTime(periodStopDatePicker.getValue()),
                resolutionChoiceBox.getValue());
    }

    private void updateActiveController(Portfolio portfolio, LocalDate startDate, LocalDate stopDate, String resolution) {
        activeController.setupPane(portfolio, startDate, stopDate, resolution);
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
        periodStartDatePicker.setValue(jodaTimeDateToJavaDate(portfolio.getEarliestDate()));
        periodStopDatePicker.setValue(jodaTimeDateToJavaDate(new LocalDate()));
        initializeCenterBorderPane();
        periodStartDatePicker.setOnAction(event -> updateActiveController(this.portfolio,
                javaDateToJodaTime(periodStartDatePicker.getValue()),
                javaDateToJodaTime(periodStopDatePicker.getValue()),
                resolutionChoiceBox.getValue()));

        periodStopDatePicker.setOnAction(event -> updateActiveController(this.portfolio,
                javaDateToJodaTime(periodStartDatePicker.getValue()),
                javaDateToJodaTime(periodStopDatePicker.getValue()),
                resolutionChoiceBox.getValue()));

        resolutionChoiceBox.getSelectionModel().selectedIndexProperty().addListener(
                (observableValue, oldValue, newValue) -> updateActiveController(this.portfolio,
                        javaDateToJodaTime(periodStartDatePicker.getValue()),
                        javaDateToJodaTime(periodStopDatePicker.getValue()),
                        resolutionChoiceBox.getItems().get(newValue.intValue())));
    }

    // TODO Move to util class
    private LocalDate javaDateToJodaTime(java.time.LocalDate fromDate) {
        return new LocalDate(fromDate.getYear(), fromDate.getMonthValue(), fromDate.getDayOfMonth());
    }

    // TODO Move to util class
    private java.time.LocalDate jodaTimeDateToJavaDate(LocalDate fromDate) {
        return java.time.LocalDate.of(fromDate.getYear(), fromDate.getMonthOfYear(), fromDate.getDayOfMonth());
    }

    public void savePortfolio() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JSON Files", "*.json"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        File selectedFile = fileChooser.showSaveDialog(null);
        if (selectedFile != null) {
            SaveUtils.saveJsonStringToFile(portfolio, selectedFile.toString());
        }
    }

    public void loadPortfolio() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JSON Files", "*.json"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        File selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile != null) {
            SaveUtils.loadJsonStringToPortfolio(selectedFile.toString());
        }
    }

    public MyLAM getMylam() {
        return mylam;
    }

    public void setMylam(MyLAM mylam) {
        this.mylam = mylam;
    }
}
