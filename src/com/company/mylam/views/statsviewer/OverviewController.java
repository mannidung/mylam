package com.company.mylam.views.statsviewer;

import com.company.mylam.portfolio.Portfolio;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;

/**
 * Created by mannidung on 14/09/14.
 */
public class OverviewController extends AbstractBorderPaneController{

    @FXML
    private LineChart<String, Number> valueChart;

    @FXML
    private CategoryAxis valueChartXAxis;

    @FXML
    private NumberAxis valueChartYAxis;

    @FXML
    private Label valueStartDateLabel;

    @FXML
    private Label valueStopDateLabel;

    @FXML
    private Label valueChangeLabel;

    @FXML
    private Label investedAmountLabel;

    @FXML
    private Label profitLabel;

    @FXML
    private Label profitPercentualLabel;

    @FXML
    private CheckBox plotShareValueCheckBox;

    private Portfolio portfolio;
    private boolean plotShareValue = false;

    // Variables for updating the chart
    private LocalDate currentStartDate;
    private LocalDate currentStopDate;
    private String currentResolution;

    // Variables for formatting and so
    DecimalFormat df = new DecimalFormat("#.00");

    public void initialize() {

    }

    private void totalValueLineGraph(LocalDate startDate, LocalDate stopDate, String resolution) {
        System.out.println(resolution);

        // Define a series
        XYChart.Series series = new XYChart.Series<String, Number>();
        series.setName("My Portfolio");
        valueChart.setTitle("Total value of portfolio");
        valueChart.setLegendVisible(false);

        double value = 0;
        double minValue = 0;
        double maxValue = 0;
        if (resolution.equals("Yearly")) {
            //TODO Still to be implemented
        } else if (resolution.equals("Monthly")) {
            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("y-M");

            // Gets the end of the first month
            LocalDate endOfFirstMonth = startDate.dayOfMonth().withMaximumValue();
            if (!plotShareValue) {
                value = portfolio.getTotalValue(startDate);
            } else {
                value = portfolio.getValueOfShares(startDate);
            }
            series.getData().add(new XYChart.Data<String, Number>(startDate.toString(dateFormatter), value));

            // Prepare for the range
            minValue = value;
            maxValue = value;

            // Generate data
            int i = 0;
            while (startDate.plusMonths(i).isBefore(stopDate)) {
                if (!plotShareValue) {
                    value = portfolio.getTotalValue(endOfFirstMonth.plusMonths(i));
                } else {
                    value = portfolio.getValueOfShares(endOfFirstMonth.plusMonths(i));
                }
                series.getData().add(new XYChart.Data<String, Number>(endOfFirstMonth.plusMonths(
                        i).toString(dateFormatter), value));
                if (value < minValue) {
                    minValue = value;
                }
                if (value > maxValue) {
                    maxValue = value;
                }
                i++;
            }
            if (!plotShareValue) {
                value = portfolio.getTotalValue(stopDate);
            } else {
                value = portfolio.getValueOfShares(stopDate);
            }
            series.getData().add(new XYChart.Data<String, Number>(stopDate.toString(dateFormatter),
                    value));

            if (value < minValue) {
                minValue = value;
            }
            if (value > maxValue) {
                maxValue = value;
            }

            for (XYChart.Data<String, Number> d : (ObservableList<XYChart.Data<String, Number>>)series.getData()) {
                d.setNode(
                        new HoveredThresholdNode(d.getXValue().toString(), d.getYValue())
                );
            }

        } else if (resolution.equals("Weekly")) {
            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("y-w");

            // Get the end of the first week
            LocalDate endOfFirstWeek = startDate.dayOfWeek().withMaximumValue();

            if (!plotShareValue) {
                value = portfolio.getTotalValue(startDate);
            } else {
                value = portfolio.getValueOfShares(startDate);
            }
            series.getData().add(new XYChart.Data(startDate.toString(dateFormatter),
                    value));

            // Prepare for the range
            minValue = value;
            maxValue = value;

            // Generate data
            int i = 0;
            while (startDate.plusWeeks(i).isBefore(stopDate)) {
                if (!plotShareValue) {
                    value = portfolio.getTotalValue(endOfFirstWeek.plusWeeks(i));
                } else {
                    value = portfolio.getValueOfShares(endOfFirstWeek.plusWeeks(i));
                }
                series.getData().add(new XYChart.Data(endOfFirstWeek.plusWeeks(i).toString(dateFormatter),
                        value));
                if (value < minValue) {
                    minValue = value;
                }
                if (value > maxValue) {
                    maxValue = value;
                }
                i++;
            }
            if (!plotShareValue) {
                value = portfolio.getTotalValue(stopDate);
            } else {
                value = portfolio.getValueOfShares(stopDate);
            }
            series.getData().add(new XYChart.Data(stopDate.toString(dateFormatter),
                    value));

            if (value < minValue) {
                minValue = value;
            }
            if (value > maxValue) {
                maxValue = value;
            }

            for (XYChart.Data<String, Number> d : (ObservableList<XYChart.Data<String, Number>>)series.getData()) {
                d.setNode(
                        new HoveredThresholdNode(d.getXValue().toString(), d.getYValue())
                );
            }

        } else if (resolution.equals("Daily")) {
            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("y-M-d");

            // Generate data
            int i = 0;

            // Prepare for the range
            if (!plotShareValue) {
                minValue = portfolio.getTotalValue(startDate.plusDays(i));
            } else {
                minValue = portfolio.getValueOfShares(startDate.plusDays(i));
            }
            maxValue = minValue;

            while (startDate.plusDays(i).isBefore(stopDate)) {
                if (!plotShareValue) {
                    value = portfolio.getTotalValue(startDate.plusDays(i));
                } else {
                    value = portfolio.getValueOfShares(startDate.plusDays(i));
                }
                series.getData().add(new XYChart.Data(startDate.plusDays(i).toString(dateFormatter),
                        value));
                if (value < minValue) {
                    minValue = value;
                }
                if (value > maxValue) {
                    maxValue = value;
                }
                i++;
            }
            if (!plotShareValue) {
                value = portfolio.getTotalValue(stopDate);
            } else {
                value = portfolio.getValueOfShares(startDate.plusDays(i));
            }
            series.getData().add(new XYChart.Data(stopDate.toString(dateFormatter),
                    value));

            if (value < minValue) {
                minValue = value;
            }
            if (value > maxValue) {
                maxValue = value;
            }

            for (XYChart.Data<String, Number> d : (ObservableList<XYChart.Data<String, Number>>)series.getData()) {
                d.setNode(
                        new HoveredThresholdNode(d.getXValue().toString(), d.getYValue())
                );
            }
        }

        valueChart.getYAxis().setAutoRanging(false);

        // padding sets the free space above and below the graph
        double padding = (maxValue-minValue)/10;

        valueChartYAxis.setLowerBound(minValue - padding);
        valueChartYAxis.setUpperBound(maxValue + padding);
        valueChartYAxis.setTickUnit((maxValue - minValue)/20);
        valueChartXAxis.setTickMarkVisible(false);

        // To keep the number of X-label entries to maximum 10
        int length = series.getData().size();
        if (length > 10) {
            String invisible = " ";
            int i = 0;
            for (XYChart.Data data : (ObservableList<XYChart.Data<String, Number>>) series.getData()) {
                if (i%(length/10) != 0) {
                    data.setXValue(invisible);
                    invisible += (char)29;
                }
                i++;
            }
        }

        valueChart.getData().add(series);
    }

    private void setLabels(LocalDate startDate, LocalDate stopDate) {
        DecimalFormat df = new DecimalFormat("#.00");
        double valueAtStartDate = portfolio.getTotalValue(startDate);
        double valueAtStopDate = portfolio.getTotalValue(stopDate);
        double changeInPeriodValue = portfolio.getChangeBetweenDates(startDate, stopDate);
        double changeInPeriodValuePercent = portfolio.getChangeBetweenDatesPercentual(startDate, stopDate);
        double investedValue = portfolio.getInvestedAmountBetweenDates(startDate, stopDate);
        double profitValue = valueAtStopDate - investedValue;
        double profitPercentual = (profitValue/investedValue)*100;

        valueStartDateLabel.setText(df.format(valueAtStartDate));
        valueStopDateLabel.setText(df.format(valueAtStopDate));

        valueChangeLabel.setText(df.format(changeInPeriodValue));
        investedAmountLabel.setText(df.format(investedValue));
        profitLabel.setText(df.format(profitValue));
        if (profitValue < 0) {
            profitLabel.setTextFill(Color.RED);
        } else if (profitValue > 0) {
            profitLabel.setTextFill(Color.GREEN);
        }

        profitPercentualLabel.setText(df.format(profitPercentual));
        if (profitPercentual < 0) {
            profitPercentualLabel.setTextFill(Color.RED);
        } else if (profitPercentual > 0) {
            profitPercentualLabel.setTextFill(Color.GREEN);
        }
    }

    public void setupPane(Portfolio portfolio, LocalDate startDate, LocalDate stopDate, String resolution) {
        currentStartDate = startDate;
        currentStopDate = stopDate;
        currentResolution = resolution;
        this.portfolio = portfolio;
        valueChart.getData().clear();
        totalValueLineGraph(startDate, stopDate, resolution);
        setLabels(startDate, stopDate);
        plotShareValueCheckBox.selectedProperty().addListener((observable, oldvalue, newvalue) ->
                shareValueCheckBoxUpdated(newvalue));
    }

    private void setupPane() {
        setupPane(portfolio, currentStartDate, currentStopDate, currentResolution);
    }

    private void shareValueCheckBoxUpdated(boolean newValue) {
        setPlotShareValue(newValue);
        setupPane();
    }

    private void setPlotShareValue(boolean plotShareValue) {
        this.plotShareValue = plotShareValue;
    }

    class HoveredThresholdNode extends StackPane {
        HoveredThresholdNode(String date, Number value) {
            setPrefSize(8, 8);

            final Label label = createDataThresholdLabel(date, value);

            setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override public void handle(MouseEvent mouseEvent) {
                    getChildren().setAll(label);
                    setCursor(Cursor.NONE);
                    toFront();
                }
            });
            setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override public void handle(MouseEvent mouseEvent) {
                    getChildren().clear();
                    setCursor(Cursor.CROSSHAIR);
                }
            });
        }

        private Label createDataThresholdLabel(String date, Number value) {
            final Label label = new Label(date + "\n"
                                    + "Value: " + df.format(value.doubleValue()));
            label.getStyleClass().addAll("default-color0", "chart-line-symbol", "chart-series-line");
            label.setStyle("-fx-font-size: 11;");

            /*
            if (priorValue == 0) {
                label.setTextFill(Color.DARKGRAY);
            } else if (value > priorValue) {
                label.setTextFill(Color.FORESTGREEN);
            } else {
                label.setTextFill(Color.FIREBRICK);
            }
            */

            label.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
            return label;
        }
    }
}
