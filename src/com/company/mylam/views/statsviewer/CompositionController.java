package com.company.mylam.views.statsviewer;

import com.company.mylam.portfolio.Portfolio;
import com.company.mylam.portfolio.fund.Fund;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedAreaChart;
import javafx.scene.chart.XYChart;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by mannidung on 14/09/14.
 */
public class CompositionController extends AbstractBorderPaneController {
    @FXML
    private StackedAreaChart<String, Number> percentagePerFundStackedAreaChart;

    @FXML
    private CategoryAxis percentagePerFundStackedAreaChartXAxis;

    @FXML
    private NumberAxis percentagePerFundStackedAreaChartYAxis;


    private Portfolio portfolio;

    public void initialize() {

    }

    private void percentagePerFundChart(LocalDate startDate, LocalDate stopDate) {
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("y-M-d");

        //percentagePerFundStackedAreaChart.getData().clear();
        percentagePerFundStackedAreaChart.setTitle("Percentage of fund in Portfolio");
        percentagePerFundStackedAreaChart.setLegendVisible(true);
        for (Fund f : portfolio) {
            if (f.getShouldPlot()) {
                // Define a series
                XYChart.Series series = new XYChart.Series();
                series.setName(f.getFundName());

                // Gets the end of the first month
                LocalDate endOfFirstMonth = startDate.dayOfMonth().withMaximumValue();

                series.getData().add(new XYChart.Data(startDate.toString(dateFormatter),
                        portfolio.getPercentageOfFundAtDate(endOfFirstMonth, f)));

                // Generate data
                int i = 0;
                while (startDate.plusMonths(i).isBefore(stopDate)) {
                    series.getData().add(new XYChart.Data(endOfFirstMonth.plusMonths(
                            i).toString(dateFormatter), portfolio.getPercentageOfFundAtDate(endOfFirstMonth.plusMonths(i), f)));
                    i++;
                }
                percentagePerFundStackedAreaChartYAxis.setAutoRanging(false);
                percentagePerFundStackedAreaChartYAxis.setLowerBound(0);
                percentagePerFundStackedAreaChartYAxis.setUpperBound(110);
                percentagePerFundStackedAreaChart.getData().add(series);
            }
        }
    }

    public void setupPane(Portfolio portfolio, LocalDate startDate, LocalDate stopDate, String resolution) {
        this.portfolio = portfolio;
        percentagePerFundStackedAreaChart.getData().clear();
        percentagePerFundChart(startDate, stopDate);
    }
}
