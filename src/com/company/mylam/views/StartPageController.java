package com.company.mylam.views;

import com.company.mylam.MyLAM;
import com.company.mylam.portfolio.Portfolio;
import com.company.mylam.portfolio.fund.Fund;
import com.company.mylam.portfolio.saveclasses.SaveUtils;
import javafx.stage.FileChooser;
import org.joda.time.LocalDate;

import java.io.File;

/**
 * Created by mannidung on 08/10/14.
 */
public class StartPageController {

    private MyLAM mylam;
    private Portfolio portfolio;

    public void initialize() {

    }

    // Wrapper method for mylam.newPortfolio();
    public void newPortfolio(){
        mylam.newPortfolio();
    }

    // Wrapper method for mylam.loadPortfolio();
    public void loadPortfolio(){
        mylam.loadPortfolio();
    }

    /**
     * Set the portfolio of the controller
     * @param portfolio portfolio of the controller
     */
    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    /**
     * Set the reference to the main class
     * @param mylam the main class
     */
    public void setMylam(MyLAM mylam){
        this.mylam = mylam;
    }
}
