package com.company.mylam.portfolio;

import com.company.mylam.portfolio.fund.Fund;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class PortfolioTest {
    String name;
    Portfolio portfolio;

    final double delta = 1e-3;

    @Before
    public void setUp() throws Exception {
        name = "My Portfolio";
        portfolio = new Portfolio(name);
    }

    @Test
    public void testConstructor() {
        assertTrue(portfolio.getName().equals(name));
    }

    @Test
    public void addRemoveFund() {
        // Test that adding and removing funds works as planned.
        LocalDate startDate1 = new LocalDate(2010, 1, 1);
        LocalDate startDate2 = new LocalDate(2009, 1, 1);
        LocalDate startDate3 = new LocalDate(2011, 1, 1);
        LocalDate thisDate;
        Fund fund1 = new Fund("Fund 1", "DE12345", 0.0, 0.0, startDate1);
        Fund fund2 = new Fund("Fund 2", "DE12346", 0.0, 0.0, startDate2);
        Fund fund3 = new Fund("Fund 3", "DE12347", 0.0, 0.0, startDate3);

        // Test that adding fund adds it to list, and sets earliestDate to the startDate of the fund
        portfolio.add(fund1);
        assertTrue(portfolio.contains(fund1));
        assertTrue(fund1.getStartDate().isEqual(portfolio.getEarliestDate()));

        // Test that adding a second fund (starting earlier) adds it to list, and sets
        // earliest date to the startDate of the fund
        portfolio.add(fund2);
        assertTrue(portfolio.contains(fund2));
        assertTrue(fund2.getStartDate().isEqual(portfolio.getEarliestDate()));

        // Test that adding a third fund (starting later) adds it to list, but keeps
        // earliest date at startDate of fund2
        portfolio.add(fund3);
        assertTrue(portfolio.contains(fund3));
        assertTrue(fund2.getStartDate().isEqual(portfolio.getEarliestDate()));

        // Remove fund 2, make sure it isn't in the list anymore, and make sure that
        // earliest date is set to that of fund 1
        portfolio.remove(fund2);
        assertFalse(portfolio.contains(fund2));
        assertTrue(fund1.getStartDate().isEqual(portfolio.getEarliestDate()));

        // Remove fund 3, make sure it isn't in the list anymore, and make sure that
        // earliest date is set to that of fund 1
        portfolio.remove(fund3);
        assertFalse(portfolio.contains(fund3));
        assertTrue(fund1.getStartDate().isEqual(portfolio.getEarliestDate()));

        // Remove fund 1, make sure it isn't in the list anymore, and make sure that
        // earliest date is set to that of today
        portfolio.remove(fund1);
        assertFalse(portfolio.contains(fund1));
        thisDate = new LocalDate();
        assertTrue(thisDate.isEqual(portfolio.getEarliestDate()));

        // Remove fund 1, make sure that no exception is thrown
        try {
            portfolio.remove(fund1);
        } catch (Exception e) {
            fail("Exception was thrown when none was expected: " + e);
        }
    }

    @Test
    public void testTotalValue() {
        LocalDate startDate1 = new LocalDate(2010, 1, 1);
        LocalDate startDate2 = new LocalDate(2009, 1, 1);
        LocalDate startDate3 = new LocalDate(2011, 1, 1);
        LocalDate thisDate;
        Fund fund1;
        Fund fund2;
        Fund fund3;

        double totalValue;
        double totalValue_check;

        // Test with 3 different
        fund1 = new Fund("Fund 1", "DE12345", 0.0, 0.0, startDate1);
        fund2 = new Fund("Fund 2", "DE12346", 0.0, 0.0, startDate1);
        fund3 = new Fund("Fund 3", "DE12347", 0.0, 0.0, startDate1);

        portfolio.add(fund1);
        portfolio.add(fund2);
        portfolio.add(fund3);

        // Test setting directly in the fund
        fund1.setNumberOfShares(startDate1, 10);
        fund1.setValueOfShare(startDate1, 100);
        fund2.setNumberOfShares(startDate1, 20);
        fund2.setValueOfShare(startDate1, 200);
        fund3.setNumberOfShares(startDate1, 30);
        fund3.setValueOfShare(startDate1, 300);

        totalValue = fund1.getTotalValueOfAsset(startDate1) +
                fund2.getTotalValueOfAsset(startDate1) +
                fund3.getTotalValueOfAsset(startDate1);
        totalValue_check = portfolio.getTotalValue(startDate1);
        assertEquals(totalValue, totalValue_check, delta);

        // Test setting over portfolio
        for (Fund f : portfolio) {
            f.setNumberOfShares(startDate1, 1);
            f.setValueOfShare(startDate1, 10);
        }
        totalValue = fund1.getTotalValueOfAsset(startDate1) +
                fund2.getTotalValueOfAsset(startDate1) +
                fund3.getTotalValueOfAsset(startDate1);
        totalValue_check = portfolio.getTotalValue(startDate1);
        assertEquals(totalValue, totalValue_check, delta);


    }

    @After
    public void tearDown() throws Exception {

    }
}