package com.company.mylam.portfolio.fund;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class FundTest {
    private Fund testFund1;
    private final double delta = 1e-3;

    private final String fundName = "TestFund";
    private final String isin = "DE12345";
    private final double managementFee = 0.0;
    private final double agio = 0.0;
    private final LocalDate startDate = new LocalDate(2000, 1, 1);

    @Before
    public void setUp() throws Exception {
        testFund1 = new Fund(fundName, isin, managementFee, agio, startDate);
    }

    @Test
    public void testConstructor() {
        assertEquals(fundName, testFund1.getFundName());
        assertEquals(isin, testFund1.getIsin());
        assertEquals(managementFee, testFund1.getManagementFee(), delta);
        assertEquals(agio, testFund1.getAgio(), delta);
        assertTrue(startDate.isEqual(testFund1.getStartDate()));
        assertEquals((Integer) 0, testFund1.getUpToDateUntil());
    }

    // Test that the value of shares are set correctly
    @Test
    public void testValueOfShare(){

        // For start date
        LocalDate dateToSetAt = testFund1.getStartDate();
        LocalDate dateToGetAt = dateToSetAt;
        LocalDate dateToRemoveAt;
        double valueOfShare = 10;
        double valueOfShare_Check;
        testFund1.setValueOfShare(dateToSetAt, valueOfShare);
        valueOfShare_Check = testFund1.getValueOfShare(dateToGetAt);
        assertEquals(valueOfShare, valueOfShare_Check, delta);

        // For other date
        dateToSetAt = dateToSetAt.plusMonths(2);
        dateToGetAt = dateToSetAt;
        valueOfShare = 20;
        testFund1.setValueOfShare(dateToSetAt, valueOfShare);
        valueOfShare_Check = testFund1.getValueOfShare(dateToGetAt);
        assertEquals(valueOfShare, valueOfShare_Check, delta);

        // For date between start date and a date where the value is set
        dateToGetAt = testFund1.getStartDate().plusMonths(1);
        valueOfShare = 10;
        valueOfShare_Check = testFund1.getValueOfShare(dateToGetAt);
        assertEquals(valueOfShare, valueOfShare_Check, delta);

        // Check that setting and getting with a date earlier than start date gives an error
        dateToSetAt = testFund1.getStartDate().minusDays(1);
        dateToGetAt = dateToSetAt;
        valueOfShare = 10;
        try {
            testFund1.setValueOfShare(dateToSetAt, valueOfShare);
            fail("Didn't catch an exception when one should have been caught...");
        } catch (IllegalArgumentException e) {

        }
        try {
            testFund1.getValueOfShare(dateToGetAt);
            fail("Didn't catch an exception when one should have been caught...");
        } catch (IllegalArgumentException e) {

        }

        // Test removal of value of share. When removing the value at
        // startdate + 2 months, that value should equal to the value
        // at the start date.
        dateToRemoveAt = testFund1.getStartDate().plusMonths(2);
        dateToGetAt = dateToRemoveAt;
        valueOfShare = 10;
        testFund1.removeValueOfShare(dateToRemoveAt);
        valueOfShare_Check = testFund1.getValueOfShare(dateToRemoveAt);
        assertEquals(valueOfShare, valueOfShare_Check, delta);

        // Test that setting a negative number of shares throws an exception
        dateToSetAt = testFund1.getStartDate();
        valueOfShare = -1.0;
        try {
            testFund1.setValueOfShare(dateToSetAt, valueOfShare);
            fail("No exception was thrown when expected.");
        } catch (IllegalArgumentException e) {

        }

        // Create a row of values of shares, check their value, and then clear the
        // value of shares map and check that all values equal 0.
        dateToSetAt = testFund1.getStartDate();
        for (int i = 0; i < 10; i++) {
            testFund1.setValueOfShare(dateToSetAt.plusMonths(i), (double) i);
            valueOfShare_Check = testFund1.getValueOfShare(dateToGetAt.plusMonths(i));
            assertEquals((double) i, valueOfShare_Check, delta);
        }
        testFund1.clearValueOfShare();
        for (int i = 0; i < 10; i++) {
            valueOfShare_Check = testFund1.getValueOfShare(dateToGetAt.plusMonths(i));
            assertEquals(0, valueOfShare_Check, delta);
        }
        testFund1.clear();
    }

    // Test that the number of shares are set correctly
    @Test
    public void testNumberOfShares(){

        // For start date
        LocalDate dateToSetAt = testFund1.getStartDate();
        LocalDate dateToGetAt = dateToSetAt;
        LocalDate dateToRemoveAt;
        double numberOfShares = 10;
        double numberOfShares_Check;
        testFund1.setNumberOfShares(dateToSetAt, numberOfShares);
        numberOfShares_Check = testFund1.getNumberOfShares(dateToGetAt);
        assertEquals(numberOfShares, numberOfShares_Check, delta);

        // For other date
        dateToSetAt = dateToSetAt.plusMonths(2);
        dateToGetAt = dateToSetAt;
        numberOfShares = 20;
        testFund1.setNumberOfShares(dateToSetAt, numberOfShares);
        numberOfShares_Check = testFund1.getNumberOfShares(dateToGetAt);
        assertEquals(numberOfShares, numberOfShares_Check, delta);

        // For date between start date and a date where the value is set
        dateToGetAt = testFund1.getStartDate().plusMonths(1);
        numberOfShares = 10;
        numberOfShares_Check = testFund1.getNumberOfShares(dateToGetAt);
        assertEquals(numberOfShares, numberOfShares_Check, delta);

        // Check that setting and getting with a date earlier than start date gives an error
        dateToSetAt = testFund1.getStartDate().minusDays(1);
        dateToGetAt = dateToSetAt;
        numberOfShares = 10;
        try {
            testFund1.setNumberOfShares(dateToSetAt, numberOfShares);
            fail("Didn't catch an exception when one should have been caught...");
        } catch (IllegalArgumentException e) {

        }
        try {
            testFund1.getNumberOfShares(dateToGetAt);
            fail("Didn't catch an exception when one should have been caught...");
        } catch (IllegalArgumentException e) {

        }

        // Test removal of value of share. When removing the value at
        // startdate + 2 months, that value should equal to the value
        // at the start date.
        dateToRemoveAt = testFund1.getStartDate().plusMonths(2);
        dateToGetAt = dateToRemoveAt;
        numberOfShares = 10;
        testFund1.removeNumberOfShares(dateToRemoveAt);
        numberOfShares_Check = testFund1.getNumberOfShares(dateToRemoveAt);
        assertEquals(numberOfShares, numberOfShares_Check, delta);

        // Test that setting a negative number of shares throws an exception
        dateToSetAt = testFund1.getStartDate();
        numberOfShares = -1;
        try {
            testFund1.setNumberOfShares(dateToSetAt, numberOfShares);
            fail("No exception was thrown when expected.");
        } catch (IllegalArgumentException e) {

        }

        // Create a row of values of shares, check their value, and then clear the
        // value of shares map and check that all values equal 0.
        dateToSetAt = testFund1.getStartDate();
        for (int i = 0; i < 10; i++) {
            testFund1.setNumberOfShares(dateToSetAt.plusMonths(i), i);
            numberOfShares_Check = testFund1.getNumberOfShares(dateToGetAt.plusMonths(i));
            assertEquals((double) i, numberOfShares_Check, delta);
        }
        testFund1.clearNumberOfShares();
        for (int i = 0; i < 10; i++) {
            numberOfShares_Check = testFund1.getNumberOfShares(dateToGetAt.plusMonths(i));
            assertEquals(0, numberOfShares_Check, delta);
        }

        testFund1.clear();
    }

    @Test
    public void testTotalValueOfAsset() {
        // This function also tests the handling of the variable "upToDateUntil",
        // since it is needed for the computation of the total value

        testFund1.clearValueOfShare();
        testFund1.clearNumberOfShares();


        LocalDate dateToAdd;
        double valueToAdd;
        int sharesToAdd;
        int intervalLength;

        // TEST OF FUNCTIONALITY AT START DATE
        // Test that it gives a total value of 0 if nothing is set
        // Test that upToDateUntil is equal to start date in all cases
        dateToAdd = testFund1.getStartDate();
        assertEquals(0, testFund1.getTotalValueOfAsset(dateToAdd), delta);
        assertTrue(testFund1.getUpToDateUntil() == 0);

        // Test that adding value but not a share gives a total value of 0
        valueToAdd = 10;
        testFund1.setValueOfShare(dateToAdd, valueToAdd);
        assertEquals(0, testFund1.getTotalValueOfAsset(dateToAdd), delta);
        assertTrue(testFund1.getUpToDateUntil() == 0);
        testFund1.clearValueOfShare();

        // Test that adding a share but not a value gives a total value of 0
        sharesToAdd = 10;
        testFund1.setNumberOfShares(dateToAdd, sharesToAdd);
        assertEquals(0, testFund1.getTotalValueOfAsset(dateToAdd), delta);
        assertTrue(testFund1.getUpToDateUntil() == 0);
        testFund1.clearNumberOfShares();

        // Test that adding one share and a value gives a total value of valueToAdd
        valueToAdd = 10;
        sharesToAdd = 1;
        testFund1.setNumberOfShares(dateToAdd, sharesToAdd);
        testFund1.setValueOfShare(dateToAdd, valueToAdd);
        assertEquals(valueToAdd*sharesToAdd, testFund1.getTotalValueOfAsset(dateToAdd), delta);
        assertTrue(testFund1.getUpToDateUntil() == 0);
        testFund1.clearNumberOfShares();
        testFund1.clearValueOfShare();

        // Test that adding one share and a value gives a total value of valueToAdd at
        // one month in the future. Also make sure that the upToDateUntil is updated to
        // one month in the future after computation of the total values.
        valueToAdd = 10;
        sharesToAdd = 1;
        testFund1.setValueOfShare(dateToAdd, valueToAdd);
        testFund1.setNumberOfShares(dateToAdd, sharesToAdd);
        assertEquals(valueToAdd*sharesToAdd, testFund1.getTotalValueOfAsset(dateToAdd.plusMonths(1)), delta);
        intervalLength = Days.daysBetween(dateToAdd.toDateTimeAtStartOfDay(),
                dateToAdd.plusMonths(1).toDateTimeAtStartOfDay()).getDays();
        assertTrue(testFund1.getUpToDateUntil() == intervalLength);

        // Test that getting the value of the asset at 15 days after start date does not change the
        // upToDateUntil to the new date, since no change has been made to either the value or the number
        // of shares.
        assertEquals(valueToAdd*sharesToAdd, testFund1.getTotalValueOfAsset(dateToAdd.plusDays(15)), delta);
        intervalLength = Days.daysBetween(dateToAdd.toDateTimeAtStartOfDay(),
                dateToAdd.plusMonths(1).toDateTimeAtStartOfDay()).getDays();
        assertTrue(testFund1.getUpToDateUntil() == intervalLength);

        // Test that setting the value of the asset at 15 days changes the upToDateUntil
        valueToAdd = 15;
        testFund1.getTotalValueOfAsset(dateToAdd.plusMonths(1)); // Reset the upToDateUntil
        testFund1.setValueOfShare(dateToAdd.plusDays(15), valueToAdd);
        intervalLength = Days.daysBetween(dateToAdd.toDateTimeAtStartOfDay(),
                dateToAdd.plusDays(15).toDateTimeAtStartOfDay()).getDays();
        assertTrue(testFund1.getUpToDateUntil() == intervalLength);


        // Test that setting the number of shares at 15 days changes the upToDateUntil
        sharesToAdd = 1;
        testFund1.getTotalValueOfAsset(dateToAdd.plusMonths(1)); // Reset the upToDateUntil
        testFund1.setNumberOfShares(dateToAdd.plusDays(15), sharesToAdd);
        intervalLength = Days.daysBetween(dateToAdd.toDateTimeAtStartOfDay(),
                dateToAdd.plusDays(15).toDateTimeAtStartOfDay()).getDays();
        assertTrue(testFund1.getUpToDateUntil() == intervalLength);

        // Test that asking for a date before startDate gives a value of 0
        assertEquals(0, testFund1.getTotalValueOfAsset(testFund1.getStartDate().minusDays(1)), delta);

        // Clean up
        testFund1.clear();

        // Check that changing only the value of the shares gives correct result
        dateToAdd = testFund1.getStartDate();
        valueToAdd = 10;
        sharesToAdd = 1;
        testFund1.setNumberOfShares(dateToAdd, sharesToAdd);
        testFund1.setValueOfShare(dateToAdd, valueToAdd);
        valueToAdd = 20;
        testFund1.setValueOfShare(dateToAdd.plusMonths(1), valueToAdd);
        assertEquals(valueToAdd, testFund1.getTotalValueOfAsset(dateToAdd.plusMonths(1)), delta);

        // Clean up
        testFund1.clear();

        // Check that changing only the number of the shares gives correct result
        dateToAdd = testFund1.getStartDate();
        valueToAdd = 10;
        sharesToAdd = 1;
        testFund1.setNumberOfShares(dateToAdd, sharesToAdd);
        testFund1.setValueOfShare(dateToAdd, valueToAdd);
        sharesToAdd = 2;
        testFund1.setNumberOfShares(dateToAdd.plusMonths(1), sharesToAdd);
        assertEquals(valueToAdd*sharesToAdd, testFund1.getTotalValueOfAsset(dateToAdd.plusMonths(1)), delta);

        // Clean up
        testFund1.clear();
    }

    @Test
    public void testValueChangeBetweenDates() {
        LocalDate date1 = testFund1.getStartDate().plusMonths(1);
        LocalDate date2 = testFund1.getStartDate().plusMonths(2);
        LocalDate date3 = testFund1.getStartDate().plusMonths(3);
        int numberOfShares1 = 1;
        double value1 = 10;
        double value2 = 20;
        double value3 = 15;
        double returnedValue;

        // Set number of shares at date1
        testFund1.setNumberOfShares(date1, numberOfShares1);
        testFund1.setValueOfShare(date1, value1);

        // Make sure that the difference between startDate and date1 is equal to value1
        returnedValue = testFund1.getChangeBetweenDates(testFund1.getStartDate(), date1);
        assertEquals(value1, returnedValue, delta);

        // Set value of shares at date 2
        testFund1.setValueOfShare(date2, value2);

        // Make sure that the difference between startDate and date2 is equal to value1 + value2
        // Make sure that the difference between date1 and date2 is equal to value2 - value1
        returnedValue = testFund1.getChangeBetweenDates(testFund1.getStartDate(), date2);
        assertEquals(value2, returnedValue, delta);
        returnedValue = testFund1.getChangeBetweenDates(date1, date2);
        assertEquals(value2 - value1, returnedValue, delta);

        // Set value of shares at date 3
        testFund1.setValueOfShare(date3, value3);

        // Make sure that the difference between startDate and date3 is equal to value 3
        // Make sure that the difference between date2 and date3 is equal to value3 - value2
        returnedValue = testFund1.getChangeBetweenDates(testFund1.getStartDate(), date3);
        assertEquals(value3, returnedValue, delta);
        returnedValue = testFund1.getChangeBetweenDates(date2, date3);
        assertEquals(value3 - value2, returnedValue, delta);



        // Make sure that the wrong order of the dates causes an exception
        try {
            testFund1.getChangeBetweenDates(date2, date1);
            fail("Exception not caught when expected");
        } catch (IllegalArgumentException e) {

        }
    }

    @After
    public void tearDown() throws Exception {

    }
}