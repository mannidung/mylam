package com.company.mylam.portfolio.fund;

import com.company.mylam.portfolio.saveclasses.FundDatacontainer;
import com.company.mylam.portfolio.saveclasses.TransactionDatacontainer;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by mannidung on 2014-08-14.
 */
public class Fund {

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getIsin() {
        return isin;
    }

    public double getManagementFee() {
        return managementFee;
    }

    public double getAgio() {
        return agio;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Integer getUpToDateUntil() {
        return upToDateUntil;
    }

    // Variables set at creation
    private String fundName;
    private final String isin;
    private final double managementFee;
    private final double agio;
    private LocalDate startDate;

    // Variables that changes over time
    private final TreeMap<Integer, Double> valueOfShare;
    private final TreeMap<Integer, Double> numberOfShares;
    private final TreeMap<Integer, Double> totalValueOfAsset;
    private Integer upToDateUntil;
    private boolean shouldPlot = true;

    public Fund(String fundName, String isin, double managementFee, double agio, LocalDate startDate) {

        this.fundName = fundName;
        this.isin = isin;
        this.managementFee = managementFee;
        this.agio = agio;
        this.startDate = startDate;
        this.valueOfShare = new TreeMap<Integer, Double>();
        this.numberOfShares = new TreeMap<Integer, Double>();
        this.totalValueOfAsset = new TreeMap<Integer, Double>();
        // Initialize start values as 0
        this.valueOfShare.put(0, 0.0);
        this.numberOfShares.put(0, 0.0);
        this.totalValueOfAsset.put(0, 0.0);
        this.upToDateUntil = 0;
    }


    public Fund(FundDatacontainer fundDatacontainer) {
        this.fundName = fundDatacontainer.getName();
        this.isin = fundDatacontainer.getIsin();
        this.managementFee = fundDatacontainer.getManagementFee();
        this.agio = fundDatacontainer.getAgio();

        // Read in date
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        this.startDate = formatter.parseLocalDate(fundDatacontainer.getStartDate());


        // Initiate the necessary variables
        this.valueOfShare = new TreeMap<Integer, Double>();
        this.numberOfShares = new TreeMap<Integer, Double>();
        this.totalValueOfAsset = new TreeMap<Integer, Double>();
        this.totalValueOfAsset.put(0, 0.0); // Initialize start value as 0
        this.upToDateUntil = 0;

        // Read the transactions from the fundDataContainer
        int numberOfTransactions = fundDatacontainer.getTransactions().length;
        for (int i = 0; i < numberOfTransactions; i++) {
            TransactionDatacontainer thisTransaction = fundDatacontainer.getTransactions()[i];
            LocalDate dateOfTransaction = formatter.parseLocalDate(thisTransaction.getTransactionDate());
            if (thisTransaction.getNumberOfShares() < 0) {
                this.setValueOfShare(dateOfTransaction, thisTransaction.getValueOfShare());
            } else if (thisTransaction.getValueOfShare() < 0) {
                this.setNumberOfShares(dateOfTransaction, thisTransaction.getNumberOfShares());
            } else {
                this.setNumberOfShares(dateOfTransaction, thisTransaction.getNumberOfShares());
                this.setValueOfShare(dateOfTransaction, thisTransaction.getValueOfShare());
            }
        }
    }

    public void clear() {
        this.clearNumberOfShares();
        this.clearValueOfShare();
    }

    public boolean getShouldPlot() {
        return shouldPlot;
    }

    public void setShouldPlot(boolean shouldPlot) {
        this.shouldPlot = shouldPlot;
    }

    public double getTotalValueOfAsset(LocalDate date) {
        int toDate = this.getDaysBetween(this.startDate, date);

        try {
            computeValueOfFund(toDate);
        } catch (IllegalArgumentException e) {
            // Asked for an invalid date, will give back a value of 0
            return 0;
        }

        return this.totalValueOfAsset.get(toDate);
    }

    private void computeValueOfFund(int toDate) throws IllegalArgumentException {
        if (toDate < 0) {
            throw new IllegalArgumentException("Date cannot be set earlier than start date of fund: " + this.startDate.toString());
        }
        // If toDate is before the upToDate, nothing needs to be done
        if (toDate < this.getUpToDateUntil()) {
            return;
        }
        // If upToDate is equal to start date, the whole computation has to be done again.
        // If else, set valueOfFund to upToDate-1 and compute from there
        if(this.upToDateUntil == 0) {
            double valueOfFund;
            int i = 0;
            while (i <= toDate) {
                try {
                    valueOfFund = this.valueOfShare.floorEntry(i).getValue()*
                            this.numberOfShares.floorEntry(i).getValue();
                } catch (NullPointerException e) {
                    // If value at start date doesn't exist, then it is 0
                    valueOfFund = 0;
                }
                this.totalValueOfAsset.put(i, valueOfFund);
                i++;
            }
            this.upToDateUntil = toDate;
        } else {
            double valueOfFund;
            int i = upToDateUntil;
            while (i <= toDate) {
                valueOfFund = this.valueOfShare.floorEntry(i).getValue()*
                        this.numberOfShares.floorEntry(i).getValue();
                this.totalValueOfAsset.put(i, valueOfFund);
                i++;
            }
            this.upToDateUntil = toDate;
        }
    }

    public void setValueOfShare(LocalDate date, double value) throws IllegalArgumentException{
        if (date.isBefore(this.startDate)) {
            throw new IllegalArgumentException("Date cannot be set earlier than start date of fund: " + this.startDate.toString());
        }
        if (value < 0) {
            throw new IllegalArgumentException("Value of share cannot be set to a negative number");
        }

        // If the date where the value of the shares should be
        // set is equal to the start date, there is no need
        // to compute the length of the interval, since it is plus 0 days
        if (date.isEqual(this.startDate)){
            this.valueOfShare.put(0, value);
            this.upToDateUntil = 0;
        } else {
            int intervalLength = this.getDaysBetween(this.startDate, date);
            if (intervalLength < this.upToDateUntil) {
                this.upToDateUntil = intervalLength;
            }
            this.valueOfShare.put(intervalLength, value);
        }
    }

    public double getValueOfShare(LocalDate date) {
        if (date.isBefore(this.startDate)) {
            throw new IllegalArgumentException("Date cannot be set earlier than start date of fund: " + this.startDate.toString());
        }
        int intervalLength = this.getDaysBetween(this.startDate, date);

        return this.valueOfShare.floorEntry(intervalLength).getValue();
    }

    public void removeValueOfShare(LocalDate date) {
        int intervalLength = this.getDaysBetween(this.startDate, date);
        //this.valueOfShare.remove(intervalLength);
        this.valueOfShare.remove(intervalLength, this.getValueOfShare(date));
    }

    public void clearValueOfShare() {
        // Also clear the total value map
        this.totalValueOfAsset.clear();

        this.valueOfShare.clear();
        this.valueOfShare.put(0, 0.0);
        this.upToDateUntil = 0;
    }

    public void setNumberOfShares(LocalDate date, double number) throws IllegalArgumentException{
        if (date.isBefore(this.startDate)) {
            throw new IllegalArgumentException("Date cannot be set earlier than start date of fund: " + this.startDate.toString());
        }
        if (number < 0) {
            throw new IllegalArgumentException("Number of shares cannot be set to a negative number");
        }
        // If the date where the value of the shares should be
        // set is equal to the start date, there is no need
        // to compute the length of the interval, since it is plus 0 days
        if (date.isEqual(this.startDate)){
            this.numberOfShares.put(0, number);
            this.upToDateUntil = 0;
        } else {
            int intervalLength = this.getDaysBetween(this.startDate, date);
            if (intervalLength < this.upToDateUntil) {
                this.upToDateUntil = intervalLength;
            }
            this.numberOfShares.put(intervalLength, number);
        }
    }

    public double getNumberOfShares(LocalDate date) {
        if (date.isBefore(this.startDate)) {
            throw new IllegalArgumentException("Date cannot be set earlier than start date of fund: " + this.startDate.toString());
        }
        int intervalLength = this.getDaysBetween(this.startDate, date);

        return this.numberOfShares.floorEntry(intervalLength).getValue();
    }

    public void removeNumberOfShares(LocalDate date) {
        int intervalLength = this.getDaysBetween(this.startDate, date);
        //this.numberOfShares.remove(intervalLength);
        this.numberOfShares.remove(intervalLength,this.getNumberOfShares(date));
    }

    public void clearNumberOfShares() {
        // Also clear totalValueOfAsset
        this.totalValueOfAsset.clear();

        this.numberOfShares.clear();
        this.numberOfShares.put(0, 0.0);
        this.upToDateUntil = 0;
    }

    public double getChangeBetweenDates(LocalDate fromDate, LocalDate toDate) throws IllegalArgumentException {
        // If the date are in the wrong order, throw an IllegalArgumentException
        if (toDate.isBefore(fromDate)) {
            throw new IllegalArgumentException("toDate cannot be earlier than fromDate.");
        }
        // Get for toDate first, since this will prepare the valueOfAsset map with the values needed
        double valueAtToDate = this.getTotalValueOfAsset(toDate);
        double valueAtFromDate = this.getTotalValueOfAsset(fromDate);
        return valueAtToDate - valueAtFromDate;
    }

    public double getInvestedAmountBetweenDates(LocalDate fromDate, LocalDate toDate) {
        int intervalStart = this.getDaysBetween(this.startDate, fromDate);
        int intervalStop = this.getDaysBetween(this.startDate, toDate);

        int i = intervalStart;
        double numberOfSharesOld = 0;
        double numberOfSharesNew = 0;
        double investedAmount = 0;
        while (i < intervalStop) {
            try {
                i = this.numberOfShares.ceilingKey(i);
            } catch (NullPointerException e) {
                break;
            }
            if (i > intervalStop) {
                break;
            } else {
                numberOfSharesNew = this.numberOfShares.get(i);
                investedAmount += (numberOfSharesNew - numberOfSharesOld)*this.valueOfShare.floorEntry(i).getValue();
                numberOfSharesOld = numberOfSharesNew;
            }
            i++;
        }
        return investedAmount;
    }

    private int getDaysBetween(LocalDate fromDate, LocalDate toDate) {
        return Days.daysBetween(fromDate.toDateTimeAtStartOfDay(), toDate.toDateTimeAtStartOfDay()).getDays();
    }

    // TODO Write test this function
    public boolean numberOfSharesSetAt(LocalDate date) {
        return numberOfSharesSetAt(getDaysBetween(this.startDate, date));
    }

    // TODO Write test for this function
    public boolean valueOfShareSetAt(LocalDate date) {
        return valueOfShareSetAt(getDaysBetween(this.startDate, date));
    }

    // TODO Write test this function
    private boolean numberOfSharesSetAt(int date) {
        return this.numberOfShares.containsKey(date);
    }

    // TODO Write test for this function
    private boolean valueOfShareSetAt(int date) {
        return this.valueOfShare.containsKey(date);
    }

    public FundDatacontainer createSaveContainer() {
        // Find out length and at what dates everything is set
        ArrayList<Integer> setAtDates = new ArrayList<>();
        int numSetDates = 0;
        for (int i = 0; i <= this.upToDateUntil; i++) {
            if (numberOfSharesSetAt(i) || valueOfShareSetAt(i)) {
                setAtDates.add(i);
                numSetDates++;
            }
        }
        TransactionDatacontainer[] transactions = new TransactionDatacontainer[numSetDates];
        for (int i : setAtDates) {
            LocalDate date = this.getStartDate().plusDays(i);
            if (!numberOfSharesSetAt(i)) {
                transactions[setAtDates.indexOf(i)] = new TransactionDatacontainer(date.toString(), -1, getValueOfShare(date));
            } else if (!valueOfShareSetAt(i)) {
                transactions[setAtDates.indexOf(i)] = new TransactionDatacontainer(date.toString(), getNumberOfShares(date), -1.0);
            } else {
                transactions[setAtDates.indexOf(i)] = new TransactionDatacontainer(date.toString(), getNumberOfShares(date), getValueOfShare(date));
            }
        }

        return new FundDatacontainer(this.fundName, this.isin, this.startDate.toString(), this.managementFee, this.agio, transactions);
    }

    public String toString() {
        return this.fundName;
    }


}

