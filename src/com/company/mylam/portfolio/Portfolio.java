package com.company.mylam.portfolio;

import com.company.mylam.portfolio.fund.Fund;
import com.company.mylam.portfolio.saveclasses.FundDatacontainer;
import com.company.mylam.portfolio.saveclasses.PortfolioDatacontainer;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mannidung on 2014-08-22.
 */
public class Portfolio extends ArrayList<Fund> {
    private String name;
    //private List<Fund> fundsList;
    private LocalDate earliestDate;

    // TODO Implement total value at any given date
    // TODO Implement procentual distribution of funds at any given date
    // TODO

    public Portfolio(String name) {
        this.name = name;
    }

    public Portfolio(PortfolioDatacontainer portfolioDatacontainer) {
        this.name = portfolioDatacontainer.getName();
        this.earliestDate = new LocalDate();

        // Add funds in fund array
        int lengthOfFundArray = portfolioDatacontainer.getFunds().length;
        for (int i = 0; i < lengthOfFundArray; i++) {
            Fund f = new Fund(portfolioDatacontainer.getFunds()[i]);
            this.add(f);
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) { this.name = name; }

    @Override
    public boolean add(Fund fund) {
        try {
            if (fund.getStartDate().isBefore(earliestDate)) {
                earliestDate = fund.getStartDate();
            }
        } catch (IllegalArgumentException e) {
            // If earliestDate isn't initiated yet, set to fund.getStartDate()
            earliestDate = fund.getStartDate();
        }
        super.add(fund);
        // TODO Remove
        //fundsList.add(fund);

        return false;
    }

    public void remove(Fund fund) {
        super.remove(fund);
        // TODO Remove
        //fundsList.remove(fund);


        // If the last element was removed, set earliestDate to todays date
        if (this.size() == 0) {
            this.earliestDate = new LocalDate();
            return;
        }
        // If fund wasn't the earliest fund, do not set earliestDate
        // else, find the new earliestDate
        if (fund.getStartDate().isEqual(this.getEarliestDate())) {
            // TODO Check efficiency of this thing
            this.earliestDate = this.get(0).getStartDate();
            for (Fund f : this) {
                if (f.getStartDate().isBefore(this.earliestDate)) {
                    this.earliestDate = f.getStartDate();
                }
            }
        }


    }

    /*
    public List<Fund> getFunds() {
        return this.fundsList;
    }
    */

    public LocalDate getEarliestDate() {
        return earliestDate;
    }

    public double getTotalValue(LocalDate date) {
        double totalValue = 0;
        for (Fund f : this) {
            if (f.getShouldPlot()) {
                totalValue += f.getTotalValueOfAsset(date);
            }
        }
        return totalValue;
    }

    public double getValueOfShares(LocalDate date) {
        double valueOfShare = 0;
        for (Fund f : this) {
            if (f.getShouldPlot()) {
                try {
                    valueOfShare += f.getValueOfShare(date);
                } catch (IllegalArgumentException e) {
                    // If date is before start date, set value to 0...
                    valueOfShare += 0;
                }
            }
        }
        return valueOfShare;
    }

    // TODO IMPORTANT: Write a test for this function!
    public double getChangeBetweenDates(LocalDate fromDate, LocalDate toDate) throws IllegalArgumentException {
        double valueChange = 0;
        for (Fund f : this) {
            if (f.getShouldPlot()) {
                valueChange = valueChange + f.getChangeBetweenDates(fromDate, toDate);
            }
        }
        return valueChange;
    }

    // TODO IMPORTANT: Write a test for this function!
    public double getChangeBetweenDatesPercentual(LocalDate fromDate, LocalDate toDate) {
        double valueAtStartDate = this.getTotalValue(fromDate);
        double valueChange = this.getChangeBetweenDates(fromDate, toDate);
        return (valueChange/valueAtStartDate)*100.0;
    }

    // TODO IMPORTANT: Write a test for this method!
    public double getPercentageOfFundAtDate(LocalDate date, Fund fund) throws IllegalArgumentException {
        double valueOfFund;
        double valueOfPortfolio;
        double percentageOfFund;
        valueOfFund = fund.getTotalValueOfAsset(date);
        valueOfPortfolio = this.getTotalValue(date);
        percentageOfFund = (valueOfFund/valueOfPortfolio)*100;
        return percentageOfFund;
    }

    // TODO IMPORTANT: Write a test for this method!
    public double getInvestedAmountBetweenDates(LocalDate fromDate, LocalDate toDate) {
        double investedAmount = 0;
        for (Fund f : this) {
            if (f.getShouldPlot()) {
                investedAmount = investedAmount + f.getInvestedAmountBetweenDates(fromDate, toDate);
            }
        }
        return investedAmount;
    }

    // TODO IMPORTANT: Write a test for this method!
    /**
     * Finds the (first) Fund with the name sought.
     * @param nameSought
     * @return The Fund with the name nameSought
     */
    public Fund findFundByName(String nameSought) throws NullPointerException {
        for (Fund f : this) {
            if (f.getFundName().equals(nameSought)) {
                return f;
            }
        }
        throw new NullPointerException("No fund with name " + nameSought + " found");
    }

    public PortfolioDatacontainer createSaveContainer() {
        // Create FundDatacontainers
        FundDatacontainer[] fundDatacontainer = new FundDatacontainer[this.size()];

        int i = 0;
        for (Fund f : this) {
            fundDatacontainer[i] = f.createSaveContainer();
            i++;
        }

        return new PortfolioDatacontainer(this.getName(), fundDatacontainer);
    }

    public void updateEarlieastDate() {
        for (Fund f : this) {
            if (f.getStartDate().isBefore(this.earliestDate)) {
                this.earliestDate = f.getStartDate();
            }
        }
    }
}
