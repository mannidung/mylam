package com.company.mylam.portfolio.saveclasses;

/**
 * Created by mannidung on 25/09/14.
 */
public class TransactionDatacontainer {
    private String transactionDate;
    private double numberOfShares;
    private double valueOfShare;

    /**
     * Creates a transaction data container for saving to JSON. By definition, if any of the numberOfShares or valueOfShare is set to a negative number, this will be interpreted as "not set".
     * @param transactionDate The date of the transaction, given in number of days after funds start date
     * @param numberOfShares Number of shares at the transaction. If not set, set to negative number
     * @param valueOfShare Value of share at the transaction. If not set, set to a negative number
     */
    public TransactionDatacontainer(String transactionDate, double numberOfShares, double valueOfShare) {
        this.transactionDate = transactionDate;
        this.numberOfShares = numberOfShares;
        this.valueOfShare = valueOfShare;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public double getNumberOfShares() {
        return numberOfShares;
    }

    public double getValueOfShare() {
        return valueOfShare;
    }
}
