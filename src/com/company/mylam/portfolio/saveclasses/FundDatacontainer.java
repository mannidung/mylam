package com.company.mylam.portfolio.saveclasses;

/**
 * Created by mannidung on 25/09/14.
 */
public class FundDatacontainer {
    private String name;
    private String isin;
    private String startDate;
    private double managementFee;
    private double agio;
    private TransactionDatacontainer[] transactions;


    public FundDatacontainer(String name, String isin, String startDate, double managementFee, double agio, TransactionDatacontainer[] transactions) {
        this.name = name;
        this.isin = isin;
        this.startDate = startDate;
        this.managementFee = managementFee;
        this.agio = agio;
        this.transactions = transactions;
    }


    public String getName() {
        return name;
    }

    public String getStartDate() {
        return startDate;
    }

    public TransactionDatacontainer[] getTransactions() {
        return transactions;
    }

    public String getIsin() {
        return isin;
    }

    public double getManagementFee() {
        return managementFee;
    }

    public double getAgio() {
        return agio;
    }
}
