package com.company.mylam.portfolio.saveclasses;

import com.company.mylam.portfolio.Portfolio;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by mannidung on 27/09/14.
 */
public class SaveUtils {

    public static void saveJsonStringToFile(Portfolio portfolio, String filePath) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(portfolio.createSaveContainer());

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(filePath, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print(jsonString);
        writer.close();
    }

    public static Portfolio loadJsonStringToPortfolio(String filePath) {
        // Read JSON String from file
        String jsonString = "Nothing has been read";
        try {
            jsonString = readFile(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Gson gson = new GsonBuilder().create();
        PortfolioDatacontainer portfoliodataContainer = gson.fromJson(jsonString, PortfolioDatacontainer.class);

        return new Portfolio(portfoliodataContainer);
    }

    private static String readFile(String path)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, "UTF-8");
    }

}
