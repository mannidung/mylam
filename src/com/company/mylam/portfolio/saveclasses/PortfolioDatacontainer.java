package com.company.mylam.portfolio.saveclasses;

/**
 * Created by mannidung on 27/09/14.
 */
public class PortfolioDatacontainer {
    private String name;
    private FundDatacontainer[] funds;

    public PortfolioDatacontainer(String name, FundDatacontainer[] funds) {
        this.name = name;
        this.funds = funds;
    }


    public String getName() {
        return name;
    }

    public FundDatacontainer[] getFunds() {
        return funds;
    }
}
