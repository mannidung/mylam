package com.company.mylam;

import com.company.mylam.portfolio.Portfolio;
import com.company.mylam.portfolio.fund.Fund;
import com.company.mylam.portfolio.saveclasses.SaveUtils;
import com.company.mylam.views.RootLayoutController;
import com.company.mylam.views.StartPageController;
import com.company.mylam.views.dialogs.*;
import com.company.mylam.views.portfoliotree.PortfolioTreeController;
import com.company.mylam.views.statsviewer.StatsViewerController;
import com.company.mylam.views.statsviewer.TransactionData;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.joda.time.LocalDate;

import java.io.File;
import java.io.IOException;

public class MyLAM extends Application {

    // Decides if test cases should be set up or not
    private boolean testModeOn = true;

    // Portfolio for GUI testing
    private Portfolio portfolio;

    // The stage, root layout, and panes
    private Stage stage;
    private BorderPane rootLayout;
    private StartPageController startPageController;
    private RootLayoutController rootLayoutController;
    private PortfolioTreeController portfolioTreeController;
    private StatsViewerController statsViewerController;
    private AnchorPane portfolioTreePane;
    private BorderPane statsViewerPane;
    private BorderPane startScreenPane;

    // Variables for the savings dialog
    private String portfolioAbsolutePath;
    private String portfolioAbsoluteDirectory;
    private String portfolioFileName;

    // Variables to control if the portfolio has been changed
    private boolean portfolioChanged = false;


    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Sets up the stage
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        this.stage.setTitle("MyLAM v0.1.0");

        /*
        Prepared listeners, in case they are needed for resizing or similar
         */
        this.stage.heightProperty().addListener((observable, oldValue, newValue) -> heightChanged(observable, oldValue, newValue));
        this.stage.widthProperty().addListener((observable, oldValue, newValue) -> widthChanged(observable, oldValue, newValue));

        initiateRootLayout();
        initiateStartScreen();
        //initiatePortfolioTree();
        //initiateStatsViewer();


        //initializePortfolioOverview();
    }



    /**
     * Initiate the root layout from the scene builder file
     */
    public void initiateRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/RootLayout.fxml"));
            rootLayout = loader.load();

            // Show the scene
            Scene scene = new Scene(rootLayout);
            this.stage.setScene(scene);
            this.stage.show();

            // Set an exit listener
            scene.getWindow().setOnCloseRequest(event -> handleCloseRequest(event));

            // Save the reference
            rootLayoutController = loader.getController();
            rootLayoutController.setPortfolio(this.portfolio);
            rootLayoutController.setMylam(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initiatePortfolioTree() {
        try {
            // Load the fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/portfoliotree/PortfolioTree.fxml"));
            portfolioTreePane = loader.load();

            // Put the tree to the left in the borderpane
            rootLayout.setLeft(portfolioTreePane);
            portfolioTreePane.setPrefHeight(stage.getHeight());
            portfolioTreePane.setPrefWidth(300);

            // Save the reference
            portfolioTreeController = loader.getController();
            portfolioTreeController.setPortfolio(this.portfolio);
            portfolioTreeController.setMylam(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initiateStatsViewer() {
        try {

            rootLayoutController.setSaveAsMenuItemDisabled(false);
            rootLayoutController.setSaveMenuItemDisabled(false);

            // Load the fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/statsviewer/StatsViewer.fxml"));
            statsViewerPane = loader.load();

            // Put this borderpane in the center of the root borderpane
            rootLayout.setCenter(statsViewerPane);
            statsViewerPane.setPrefHeight(stage.getHeight() - 20);
            statsViewerPane.setPrefWidth(stage.getWidth() - portfolioTreePane.getPrefWidth());

            // Save the reference
            statsViewerController = loader.getController();
            statsViewerController.setPortfolio(this.portfolio);
            statsViewerController.setMylam(this);
            portfolioTreeController.setStatsViewerController(statsViewerController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initiateStartScreen() {
        try {
            // Load the fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/StartPage.fxml"));
            startScreenPane =
                    loader.load();

            // Put this borderpane in the center of the root borderpane
            rootLayout.setCenter(startScreenPane);
            startScreenPane.setPrefHeight(stage.getHeight() - 20);

            // Save the reference
            startPageController = loader.getController();
            startPageController.setPortfolio(this.portfolio);
            startPageController.setMylam(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean showEditTransactionDialog(TransactionData transactionData) {
        //region DEBUG
        System.out.println("showEditTransactionDialog() called...");
        //endregion

        try {
            // Load the fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/dialogs/EditTransaction.fxml"));
            GridPane pane = loader.load();

            // Create a stage, separate from the main stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit transaction");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(stage);
            Scene scene = new Scene(pane);
            dialogStage.setScene(scene);

            // Create the controller
            EditTransactionController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setPortfolio(this.portfolio);
            controller.setTransactionData(transactionData);
            controller.setupDialog();

            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showAddTransactionDialog(String fundName) {
        //region DEBUG
        System.out.println("showAddTransactionDialog() called...");
        System.out.println("A transaction will be added to the fund " + fundName);
        //endregion

        try {
            // Set up a dummy transaction data
            TransactionData transactionData = new TransactionData(LocalDate.now(),
                    portfolio.findFundByName(fundName),
                    (LocalDate.now()));

            // Load the fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/dialogs/EditTransaction.fxml"));
            GridPane pane = loader.load();

            // Create a stage, separate from the main stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Add transaction");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(stage);
            Scene scene = new Scene(pane);
            dialogStage.setScene(scene);

            // Create the controller
            EditTransactionController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setPortfolio(this.portfolio);
            controller.setTransactionData(transactionData);
            controller.setEditTransaction(false);
            controller.setupDialog();

            dialogStage.showAndWait();

            if (controller.isOkClicked()) {
                setPortfolioChanged(true);
            }
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showEditPortfolioDialog() {
        //region DEBUG
        System.out.println("showEditPortfolioDialog() called...");
        //endregion

        try {

            // Load the fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/dialogs/EditPortfolio.fxml"));
            GridPane pane = loader.load();

            // Create a stage, separate from the main stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit portfolio");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(stage);
            Scene scene = new Scene(pane);
            dialogStage.setScene(scene);

            // Create the controller
            EditPortfolioController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setPortfolio(this.portfolio);
            controller.setPortfolioTreeController(portfolioTreeController);
            controller.setupDialog();

            dialogStage.showAndWait();

            if (controller.isOkClicked()) {
                setPortfolioChanged(true);
            }
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showAddFundDialog() {
        //region DEBUG
        System.out.println("showAddFundDialog() called...");
        //endregion

        try {

            // Load the fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/dialogs/AddFund.fxml"));
            GridPane pane = loader.load();

            // Create a stage, separate from the main stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit fund");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(stage);
            Scene scene = new Scene(pane);
            dialogStage.setScene(scene);

            // Create the controller
            AddFundController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setPortfolio(this.portfolio);
            controller.setPortfolioTreeController(portfolioTreeController);
            controller.setupDialog();

            dialogStage.showAndWait();

            if (controller.isOkClicked()) {
                setPortfolioChanged(true);
            }
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showEditFundDialog(String fundName) {
        //region DEBUG
        System.out.println("showEditFundDialog() called...");
        System.out.println("The fund " + fundName + " will be edited...");
        //endregion

        try {

            // Load the fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/dialogs/EditFund.fxml"));
            GridPane pane = loader.load();

            // Create a stage, separate from the main stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit fund");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(stage);
            Scene scene = new Scene(pane);
            dialogStage.setScene(scene);

            // Create the controller
            EditFundController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setPortfolio(this.portfolio);
            controller.setPortfolioTreeController(portfolioTreeController);
            controller.setupDialog(portfolio.findFundByName(fundName));

            dialogStage.showAndWait();

            if (controller.isOkClicked()) {
                setPortfolioChanged(true);
            }
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showDeleteFundDialog(String fundName) {
        //region DEBUG
        System.out.println("showDeleteFundDialog() called...");
        System.out.println("The fund " + fundName + " will be deleted...");
        //endregion

        try {

            // Load the fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/dialogs/OkCancelDialog.fxml"));
            GridPane pane = loader.load();

            // Create a stage, separate from the main stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit fund");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(stage);
            Scene scene = new Scene(pane);
            dialogStage.setScene(scene);

            // Create the controller
            OkCancelDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setupDialog("Do you really want to delete this fund?",
                    "This action is not reversible. Once deleted, the fund is deleted... \n" +
                            "for all eternity...");

            dialogStage.showAndWait();

            if (controller.isOkClicked()) {
                setPortfolioChanged(true);
                portfolio.remove(portfolio.findFundByName(fundName));
                portfolioTreeController.setPortfolio(portfolio);
            }
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void heightChanged(ObservableValue o, Number oldValue, Number newValue) {
        try {
            statsViewerPane.setPrefHeight(stage.getHeight());
            statsViewerPane.setPrefWidth(stage.getWidth() - portfolioTreePane.getPrefWidth());
        } catch (NullPointerException e) {
            // In case that StatsViewerPane hasn't been initialized yet
        }
    }

    private void widthChanged(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
        try {
            statsViewerPane.setPrefHeight(stage.getHeight());
            statsViewerPane.setPrefWidth(stage.getWidth() - portfolioTreePane.getPrefWidth());
        } catch (NullPointerException e) {
            // In case that StatsViewerPane hasn't been initialized yet
        }
    }

    /**
     * newPortfolio creates a new portfolio, from which the user
     * can make changes and create a custom portfolio
     */
    public void newPortfolio(){
        //region DEBUG
        System.out.println("newPortfolio() called");
        //endregion

        // If the portfolio has been changed, create a dialog and query the
        // user if he wants to save the changes.
        if (portfolioChanged) {
            //region DEBUG
            System.out.println("Portfolio has changed...");
            //endregion

            // Check if the user want to ignore the unsaved changes
            if (!shouldUnsavedChangesBeIgnored()) {
                return;
            }
        }

        Fund fund = new Fund("My First Fund", "de123", 0.0, 0.0, new LocalDate());
        portfolio = new Portfolio("My Portfolio");
        portfolio.add(fund);
        setPortfolio(portfolio);
        initiatePortfolioTree();
        initiateStatsViewer();
    }

    public void savePortfolio() {
        //region DEBUG
        System.out.println("savePortfolio() called");
        //region

        try {
            File saveFile = new File(portfolioAbsolutePath);
            SaveUtils.saveJsonStringToFile(getPortfolio(), portfolioAbsolutePath);
            setPortfolioChanged(false);
        } catch (NullPointerException e) {
            //region DEBUG
            System.out.println("File at path " + portfolioAbsolutePath + " did not exist...");
            //endregion

            // Load the fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyLAM.class.getResource("views/dialogs/OkDialog.fxml"));


            GridPane pane = null;
            try {
                pane = loader.load();
            } catch (IOException err2) {
                err2.printStackTrace();
            }

            // Create a stage, separate from the main stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Error when saving");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(stage);
            Scene scene = new Scene(pane);
            dialogStage.setScene(scene);

            // Create the controller
            OkDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setupDialog("Path not found!",
                    "You don't seem to have a path set for saving your portfolio... \n \n" +
                            "Try using \"Save as...\" instead of \"Save...\"!");

            dialogStage.showAndWait();
        }
    }

    /**
     * saveAsPortfolio asks the user where to save the current portfolio
     * and writes it to file
     */
    public void saveAsPortfolio(){
        //region DEBUG
        System.out.println("saveAsPortfolio() called");
        //region

        FileChooser fileChooser = new FileChooser();

        // Set inital values
        fileChooser.setInitialDirectory(new File(portfolioAbsoluteDirectory));
        fileChooser.setInitialFileName(portfolioFileName);

        fileChooser.setTitle("Save portfolio");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JSON Files", "*.json"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        File selectedFile = fileChooser.showSaveDialog(null);
        if (selectedFile != null) {
            SaveUtils.saveJsonStringToFile(getPortfolio(), selectedFile.toString());
        }
    }

    /**
     * loadPortfolio opens a file chooser window where the user can choose an
     * already existing file for loading into mylam.
     */
    public void loadPortfolio() {
        //region DEBUG
        System.out.println("loadPortfolio() called");
        //region

        // If the portfolio has been changed, create a dialog and query the
        // user if he wants to save the changes.
        if (portfolioChanged) {
            //region DEBUG
            System.out.println("Portfolio has changed...");
            //endregion

            // Check if the user want to ignore the unsaved changes
            if (!shouldUnsavedChangesBeIgnored()) {
                return;
            }
        }

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load porfolio");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JSON Files", "*.json"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        File selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile != null) {
            // TODO Exception here?
            portfolio = SaveUtils.loadJsonStringToPortfolio(selectedFile.getPath());
        }
        File absoluteFile = selectedFile.getAbsoluteFile();
        portfolioAbsolutePath = absoluteFile.getAbsolutePath();
        portfolioAbsoluteDirectory = absoluteFile.getParent();
        portfolioFileName = absoluteFile.getName();

        //region DEBUG
        System.out.println("Absolute path of portfolio is " + portfolioAbsolutePath);
        System.out.println("Absolute directory of portfolio is " + portfolioAbsoluteDirectory);
        System.out.println("File name of portfolio is " + portfolioFileName);
        //endregion

        setPortfolio(portfolio);
        setPortfolioChanged(false);

        initiatePortfolioTree();
        initiateStatsViewer();
    }

    public void closeApplication() {
        //region DEBUG
        System.out.println("closeApplication() called");
        //region
        stage.fireEvent(
                new WindowEvent(
                stage,
                WindowEvent.WINDOW_CLOSE_REQUEST
        ));
    }

    private void handleCloseRequest(Event event) {
        //region DEBUG
        System.out.println("closeApplication(Event event) called...");
        //endregion

        if (portfolioChanged) {
            if (!shouldUnsavedChangesBeIgnored()) {
                event.consume();
            }
        }
    }

    private boolean shouldUnsavedChangesBeIgnored() {
        // Load the fxml file
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MyLAM.class.getResource("views/dialogs/OkCancelDialog.fxml"));


        GridPane pane = null;
        try {
            pane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create a stage, separate from the main stage
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Edit fund");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(stage);
        Scene scene = new Scene(pane);
        dialogStage.setScene(scene);

        // Create the controller
        OkCancelDialogController controller = loader.getController();
        controller.setDialogStage(dialogStage);
        controller.setupDialog("Unsaved changes!",
                "You have unsaved changes in your portfolio... \n" +
                        "Do you want continue without saving?");

        dialogStage.showAndWait();

        return controller.isOkClicked();
    }

    public void setPortfolio(Portfolio portfolio){
        this.portfolio = portfolio;
    }

    public Portfolio getPortfolio() { return this.portfolio; }

    public boolean isPortfolioChanged() {
        return portfolioChanged;
    }

    public void setPortfolioChanged(boolean portfolioChanged) {
        this.portfolioChanged = portfolioChanged;
    }


}
