# !!!README!!!


## Reporting issues

In order to track the issues efficiently some information is needed.

* Component - Describes what part of MyLAM that has to be changed to solve the bug. This should be set by the developers
* Milestone - The milestone at which the issue should be solved. This should be set by the developers
* Version - In what version of MyLAM does the issue occur? This should be set by the reporter
